"use client";
import React from "react";
import "../../../css/callNow.css";
import { FaPhoneAlt } from "react-icons/fa";
import { useSelector } from "react-redux";

export default function CallNow() {
  let { settingObj } = useSelector((state) => state.settingSlice);
  return (
    <div className="hotline-phone-ring-wrap">
      <div className="hotline-phone-ring">
        <div className="hotline-phone-ring-circle" />
        <div className="hotline-phone-ring-circle-fill" />
        <div className="hotline-phone-ring-img-circle">
          <a href={`tel:${settingObj.phone}`} className="pps-btn-img">
            <FaPhoneAlt fill="white" width={50} />
          </a>
        </div>
      </div>
    </div>
  );
}
