"use client";
import { useEffect } from "react";

{
  /* Zalo OA */
}

const ChatZalo = () => {
  useEffect(() => {
    const script = document.createElement("script");
    script.src = "https://sp.zalo.me/plugins/sdk.js";
    script.async = true;

    document.body.appendChild(script);

    script.onload = () => {
      // Khởi tạo plugin chat khi script đã được load
      const chatWidget = document.createElement("div");
      chatWidget.classList.add("zalo-chat-widget");
      chatWidget.setAttribute("data-oaid", "3046731547432651458");
      chatWidget.setAttribute(
        "data-welcome-message",
        "Rất vui khi được hỗ trợ bạn!"
      );
      chatWidget.setAttribute("data-autopopup", "1");
      chatWidget.setAttribute("data-width", "350");
      chatWidget.setAttribute("data-height", "420");

      document.body.appendChild(chatWidget);
    };

    // Cleanup
    return () => {
      document.body.removeChild(script);
    };
  }, []);

  return null;
};

export default ChatZalo;
