import { Spin } from "antd";
import { useSelector } from "react-redux";
import "../../../css/loadingSpinner.css";
import { LoadingOutlined } from "@ant-design/icons";
import { useEffect } from "react";
const LoadingSpinner = () => {
  let { actionSpinner } = useSelector((state) => state.settingSpinnerAction);
  useEffect(() => {
    if (actionSpinner) {
      document.body.style.overflow = "hidden";
    } else {
      document.body.style.overflow = "unset";
    }
  }, [actionSpinner]);

  return (
    <>
      {actionSpinner && (
        <Spin
          spinning={actionSpinner}
          fullscreen="true"
          indicator={
            <LoadingOutlined
              style={{
                fontSize: 24,
              }}
              spin
            />
          }
        />
      )}
    </>
  );
};

export default LoadingSpinner;
