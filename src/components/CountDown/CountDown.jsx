import React, { useEffect } from "react";
import Countdown from "react-countdown";

const CountDown = ({ openCountDown, periodTime }) => {
  const [time, setTime] = React.useState(Date.now() + periodTime);
  //render cái đồng hồ
  const renderer = ({ hours, minutes, seconds, completed }) => {
    if (!completed) {
      // Render a countdown
      return (
        <span className="mt-1 text-red-500 text-sm">
          {minutes}:{seconds < 10 ? "0" + seconds : seconds}
        </span>
      );
    }
  };

  useEffect(() => {
    setTime(Date.now() + periodTime);
  }, [openCountDown]);

  return (
    <>
      <Countdown
        date={time}
        autoStart={true}
        renderer={renderer}
        zeroPadTime={2}
      />
    </>
  );
};

export default CountDown;
