import React from "react";
import "../../../../css/IntroDetail.css";
import { useSelector } from "react-redux";

export default function IntroDetail() {
  let { settingObj } = useSelector((state) => state.settingSlice);
  return (
    <div className="introDetail ">
      <div className="container-1 tb-padding-2 space-y-7">
        {/* title */}
        <h2 className="uppercase">
          GIỚI THIỆU BAO BÌ CARTON <span className="text-main-blue">SGN</span>
        </h2>
        {/* info CÔNG TY TNHH ĐẦU TƯ THƯƠNG MẠI SÀI GÒN NGUYỄN - BAO BÌ SGN */}
        <div className="space-y-4">
          {/* title */}
          <p className="uppercase text-main-blue font-bold">
            CÔNG TY TNHH ĐẦU TƯ THƯƠNG MẠI SÀI GÒN NGUYỄN - BAO BÌ SGN
          </p>
          {/* list info */}
          <ul className="list-[square] pl-4">
            <li>
              Chuyên cung cấp và sản xuất bao bì giấy carton, với kinh nghiệm
              hơn 5 năm làm việc trong nghành sản xuất bao bì.
            </li>
            <li>
              Ngoài hơn 100 size thùng được sản xuất tồn kho sẳn để giao cho quý
              khách trong 24h. Chúng tôi còn nhận sản xuất thiết kế và in ấn
              theo yêu cầu của khách hàng và giao hàng từ 4-5 ngày làm việc.
            </li>
            <li>
              Để đáp ứng nhu cầu ngày càng tăng chúng tôi liên tục phát triển,
              đầu tư thêm nhiều máy móc hiện đại
            </li>
            <li>
              <p>Các sản phẩm bao bì chủ đạo như:</p>
              <ul className="list-disc pl-4">
                <li>
                  Thùng carton SGN chuyên phục vụ cho ngành THƯƠNG MẠI ĐIỆN TỬ (
                  luôn sẳn sàng hơn 100 size để giao ngay trong 24h )
                </li>
                <li>Máy sản xuất bong bóng khí ( màn xốp hơi )</li>
                <li>
                  Thùng carton SGN chuyên đựng hàng rau củ quả, trái cây như:
                  sầu riêng, xoài, dưa lưới..... ( được đục lổ để đảm bảo sản
                  phẩm bên trong được tốt nhất )
                </li>
                <li>
                  Chúng tôi chuyên sản xuất túi giấy kraft có quai và không quai
                  với chất lượng cam kết theo tiêu chuẩn xuất khẩu sang Mỹ,
                  EU....
                </li>
                <li>VÀ CÒN NHIỀU SẢN PHẨM CHỦ LỰC KHÁC....</li>
              </ul>
            </li>
          </ul>
        </div>
        {/* liên hệ */}
        <div className="space-y-4">
          <p className="font-bold">Mọi chi tiết xin liên hệ</p>
          <ul className="list-none">
            <li>
              <div
                dangerouslySetInnerHTML={{ __html: settingObj.address_contact }}
                className="space-y-2 xl:space-y-4"
              ></div>
            </li>
            <li>
              <p>
                Điện thoại:{" "}
                <a
                  href={`tel:${settingObj.hotline}`}
                  className="text-main-blue"
                >
                  {settingObj.hotline}
                </a>
              </p>
            </li>
            <li>
              <p>
                Zalo:{" "}
                <a
                  href={`tel:${settingObj.hotline}`}
                  className="text-main-blue"
                >
                  {settingObj.hotline}
                </a>
              </p>
            </li>
            <li>
              <p>
                Email:{" "}
                <a
                  href={`mailto:${settingObj.email}`}
                  className="text-main-blue"
                >
                  {settingObj.email}
                </a>
              </p>
            </li>
            <li>
              <p>
                Website:{" "}
                <a href="/" className="text-main-blue">
                  {settingObj.site}
                </a>
              </p>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}
