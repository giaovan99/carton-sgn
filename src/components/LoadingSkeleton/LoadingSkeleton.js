import React from "react";
import "../../../css/LoadingSkeletion.css";

export default function LoadingSkeleton() {
  return (
    <div className="loadingPage">
      <div class="loader"></div>
    </div>
  );
}
