import React from "react";
import "../../../../css/Pagination.css";
import Image from "next/image";
import arrowIcon from "../../../../public/svg-icon/arrow-down.svg";

export default function Pagination({
  totalPosts,
  postsPerPage,
  setCurrentPage,
  currentPage,
}) {
  let pages = [];
  for (let i = 1; i <= Math.ceil(totalPosts / postsPerPage); i++) {
    pages.push(i);
  }
  let handleChangePage = (num) => {
    if (num == -1) {
      if (currentPage > 1) {
        setCurrentPage(currentPage - 1);
      }
    }
    if (num == 1) {
      if (currentPage < pages.length) {
        setCurrentPage(currentPage + 1);
      }
    }
  };
  return (
    <div className="paginationCustom space-x-2 flex justify-center mt-10">
      <button
        className="paginationItem py-2"
        style={{ transform: "rotate(90deg)" }}
        onClick={() => {
          handleChangePage(-1);
        }}
      >
        <Image src={arrowIcon} alt="..." />
      </button>
      {pages.map((page, index) => {
        return (
          <button
            key={index}
            className={
              page == currentPage
                ? "paginationItem py-2 active"
                : "paginationItem py-2"
            }
            onClick={() => {
              setCurrentPage(page);
            }}
          >
            {page}
          </button>
        );
      })}
      <button
        className="paginationItem py-2"
        style={{ transform: "rotate(-90deg)" }}
        onClick={() => {
          handleChangePage(1);
        }}
      >
        <Image src={arrowIcon} alt="..." />
      </button>
    </div>
  );
}
