"use client";
import React from "react";
import { useEffect, useRef, useState } from "react";
import "../../../../../css/FilterProductListPage.css";
import downArrowIcon from "../../../../../public/svg-icon/arrow-down.svg";
import Image from "next/image";

export default function FilterProductListPage({
  selected,
  setSelected,
  options,
}) {
  const [isActive, setIsActive] = useState(false);
  let menuRef = useRef();
  useEffect(() => {
    let handler = (e) => {
      if (!menuRef.current.contains(e.target)) {
        setIsActive(false);
      }
    };
    document.addEventListener("mousedown", handler);
    return () => {
      document.removeEventListener("mousedown", handler);
    };
  }, []);

  let renderOption = () => {
    return options.map((option, index) => {
      return (
        <div
          className="dropdown-item"
          onClick={(e) => {
            setSelected(option);
            setIsActive(false);
          }}
          key={index}
        >
          {option}
        </div>
      );
    });
  };

  return (
    <div className="SelectComponent">
      <div className="dropdown" ref={menuRef}>
        <div
          className="dropdown-btn pl-[10px] flex justify-between items-center"
          onClick={(e) => setIsActive(!isActive)}
        >
          <p>{selected}</p>
          <p className="downArrowIcon ml-3">
            <Image src={downArrowIcon} alt="..." />
          </p>
        </div>

        {isActive && <div className="dropdown-content">{renderOption()}</div>}
      </div>
    </div>
  );
}
