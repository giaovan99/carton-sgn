import RenderProductItem from "@/components/RenderProductList/RenderProductItem/RenderProductItem";
import React from "react";

export default function RenderPagination({ arr }) {
  return (
    <div className="RenderPagination grid grid-cols-2 md:grid-cols-3">
      {arr.map((item) => {
        return <RenderProductItem item={item} key={item.id} />;
      })}
    </div>
  );
}
