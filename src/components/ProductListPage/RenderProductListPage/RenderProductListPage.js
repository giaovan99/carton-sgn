"use client";
import React, { useState } from "react";
import "../../../../css/RenderProductListPage.css";
import DropDownMenu from "@/components/DropDownMenu/DropDownMenu";
import RenderPagination from "./RenderPagination/RenderPagination.js";
import { Pagination } from "antd";
import FilterProductListPage from "./FilterProductListPage/FilterProductListPage";

export default function RenderProductListPage({
  arr,
  setPage,
  total,
  setArrType,
  page,
  selected,
  setSelected,
}) {
  let [options, setOptions] = useState([
    "Mặc định",
    "Giá tăng dần",
    "Giá giảm dần",
  ]);

  // funtions
  let onChange = (page) => {
    setPage(page);
    window.scrollTo(0, 0);
  };

  return (
    <div className="renderProductListPage">
      <div className="container-1 tb-padding-3 flex">
        {/* MenuDropDown */}
        <div className="left-side hidden lg:block dropDownMenuHomePage basic-1/5">
          <DropDownMenu />
        </div>
        {/* Banner */}
        <div className="right-side basis-full lg:basis-4/5 lg:pl-6">
          {/* bộ lọc */}
          <div className="filter flex justify-end ">
            <FilterProductListPage
              options={options}
              selected={selected}
              setSelected={setSelected}
            />
          </div>

          {/* render list sản phẩm */}
          <div className="renderPaginationList pt-8">
            <RenderPagination arr={arr} />
            <div className="paginationCustom flex justify-center mt-9">
              <Pagination
                defaultCurrent={1}
                total={total}
                onChange={onChange}
                defaultPageSize={12}
                hideOnSinglePage={true}
                current={page}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
