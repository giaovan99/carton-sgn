"use client";
import React, { useEffect, useState } from "react";
import phoneIcon from "../../../../public/svg-icon/call.svg";
import passIcon from "../../../../public/svg-icon/lock.svg";
import Image from "next/image";
import "../../../../css/FormForgetPasswordPage.css";
import { userService } from "@/services/customerService";
import { useDispatch } from "react-redux";
import { setDefinePageTitle } from "@/redux/slice/productSlice";
import { useFormik } from "formik";
import * as yup from "yup";
import NextPage from "@/components/NextPage/NextPage";
export default function FormForgetPasswordPage() {
  const [phoneNumber, setPhoneNumber] = useState("");
  const [forgetPassAPI, setForgetPassAPI] = useState(false);
  const [otpConfirm, setOtpConfirm] = useState("");
  const [openResult, setOpenResult] = useState(false);

  let dispatch = useDispatch();
  const formik2 = useFormik({
    initialValues: {
      phone: "",
    },
    validationSchema: yup.object({
      phone: yup
        .string()
        .required("Nhập số điện thoại")
        .matches(
          /^(?!.*[^\d+])(84|0[35789])(\d{8})\b/,
          "Số điện thoại không hợp lệ"
        ),
    }),
    onSubmit: async (values) => {
      let res = await userService.forgetPassword({ phone: values.phone });
      if (res.data.success == false) {
        formik2.setFieldError("phone", res.data.error.phone[0]);
        document.getElementById("phone").focus();
      } else {
        setOtpConfirm(res.data.message);
        formik.setFieldError("otp", res.data.message);
        setForgetPassAPI(true);
      }
    },
  });

  const formik = useFormik({
    initialValues: {
      phone: "",
      otp: "",
      password: "",
      passwordConfirm: "",
    },
    validationSchema: yup.object({
      password: yup
        .string()
        .required("Nhập mật khẩu")
        .matches(
          /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/,
          "Mật khẩu yêu cầu ít nhất 8 ký tự (ít nhất 1 ký tự thường, 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)"
        ),
      passwordConfirm: yup
        .string()
        .required("Xác nhận lại mật khẩu")
        .oneOf([yup.ref("password"), null], "Mật khẩu chưa trùng khớp"),
      otp: yup.string().required("Nhập mã OTP"),
    }),
    onSubmit: async (values) => {
      let res = await userService.changePassword(values);
      if (res.data.success) {
        setOpenResult(true);
      } else {
        formik.setFieldError("otp", res.data.message);
      }
    },
  });

  useEffect(() => {
    dispatch(setDefinePageTitle({ title: "Quên mật khẩu", id: -1 }));
  }, []);

  // regexPhoneNumber
  let regexPhoneNumber = (phone) => {
    const regex = /(84|0[3|5|7|8|9])+([0-9]{8})\b/g;
    var isValid = regex.test(phone);
    if (!isValid) {
      document.getElementById("phone_warning").classList.remove("hidden");
      document.getElementById("phone_warning").innerHTML =
        "Số điện thoại không hợp lệ";
      return false;
    } else {
      document.getElementById("phone_warning").classList.add("hidden");
      document.getElementById("phone_warning").innerHTML = "";
      return true;
    }
  };

  let checkForgotPasswordAPI = async (event) => {
    event.preventDefault();
    if (regexPhoneNumber(phoneNumber)) {
      let res = await userService.forgetPassword({ phone: phoneNumber });
      if (res.data.success == false) {
        document.getElementById("phone_warning").classList.remove("hidden");
        document.getElementById("phone_warning").innerHTML =
          res.data.error.phone[0];
        document.getElementById("phone").focus();
      } else {
        document.getElementById("phone_warning").classList.add("hidden");
        document.getElementById("phone_warning").innerHTML = "";
        setOtpConfirm(res.data.message);
        formik.setFieldError("otp", res.data.message);
        setForgetPassAPI(true);
      }
    }
  };

  return (
    <div className="formForgetPasswordPage">
      <div className="container-1 tb-padding-2">
        {forgetPassAPI == false ? (
          // Khi mới load trang, chưa nhập sdt & apiCheckSDT chưa thành công
          <form
            className="formLG flex flex-col justify-between space-y-4"
            onSubmit={(event) => {
              event.preventDefault();
              formik2.handleSubmit();
            }}
          >
            {/* title */}
            <div className="title text-center font-bold">
              <h4>Quên mật khẩu</h4>
            </div>
            {/* form */}
            <div className="userInfo space-y-4 mt-4">
              {/* input Tài khoản */}
              <div>
                <div className="inputUserInfo flex space-x-2.5 items-center">
                  <label htmlFor="phone">
                    <Image
                      src={phoneIcon}
                      alt="..."
                      className="iconInputUserInfo"
                    />
                  </label>
                  <input
                    type="text"
                    placeholder="Nhập số điện thoại"
                    name="phone"
                    id="phone"
                    className="grow"
                    onChange={(event) => {
                      setPhoneNumber(event.target.value);
                      formik.setFieldValue("phone", event.target.value);
                      formik2.setFieldValue("phone", event.target.value);
                    }}
                    required
                    value={phoneNumber}
                  />
                </div>
                {formik2.errors.phone && (
                  <p className="mt-1 text-red-500 text-sm " id="phone_warning">
                    {formik2.errors.phone}
                  </p>
                )}
              </div>
            </div>

            {/* button đăng ký */}
            <div className="forgotPasswordButton">
              <button
                className="main-border bg-main-blue w-full py-3.5 text-center font-bold text-white"
                type="submit"
              >
                Lấy mã OTP
              </button>
            </div>
          </form>
        ) : (
          // Form 2
          <form
            className="formLG flex flex-col justify-between space-y-4"
            onSubmit={(event) => {
              event.preventDefault();
              formik.handleSubmit();
            }}
          >
            {/* title */}
            <div className="title text-center font-bold">
              <h4>Quên mật khẩu</h4>
            </div>
            {/* form */}
            <div className="userInfo space-y-4 mt-4">
              <div className="space-y-3">
                {/* input Tài khoản */}
                <div className="inputUserInfo flex space-x-2.5 items-center">
                  <label htmlFor="phone">
                    <Image
                      src={phoneIcon}
                      alt="..."
                      className="iconInputUserInfo"
                    />
                  </label>
                  <input
                    type="text"
                    placeholder="Số điện thoại"
                    name="phone"
                    id="phone"
                    className="grow"
                    disabled
                    value={formik.values.phone}
                  />
                </div>

                {/* input Mật khẩu */}
                <div className="inputUserInfo flex space-x-2.5 items-center">
                  <label htmlFor="phone">
                    <Image
                      src={passIcon}
                      alt="..."
                      className="iconInputUserInfo"
                    />
                  </label>
                  <input
                    placeholder="Mật khẩu mới"
                    type="password"
                    name="password"
                    id="password"
                    className="grow"
                    onBlur={formik.handleBlur}
                    value={formik.values.password}
                    onChange={formik.handleChange}
                  />
                </div>
                {formik.errors.password && (
                  <p
                    className={`mt-1 text-sm ${
                      !formik.touched.password &&
                      formik.errors.password == "Nhập mật khẩu"
                        ? "text-blue-500"
                        : "text-red-500"
                    }`}
                    id="password_warning"
                  >
                    {formik.errors.password}
                  </p>
                )}

                {/* input Nhập lại Mật khẩu */}
                <div className="inputUserInfo flex space-x-2.5 items-center">
                  <input
                    type="password"
                    placeholder="Xác nhận lại mật khẩu"
                    name="passwordConfirm"
                    id="passwordConfirm"
                    className="grow"
                    onBlur={formik.handleBlur}
                    value={formik.values.passwordConfirm}
                    onChange={formik.handleChange}
                  />
                </div>
                {!formik.errors.password && formik.errors.passwordConfirm && (
                  <p
                    className={`mt-1 text-sm ${
                      !formik.touched.passwordConfirm &&
                      formik.errors.passwordConfirm == "Xác nhận lại mật khẩu"
                        ? "text-blue-500"
                        : "text-red-500"
                    }`}
                    id="passwordConfirm_warning"
                  >
                    {formik.errors.passwordConfirm}
                  </p>
                )}
                {/* input OTP */}
                <div className="inputUserInfo flex space-x-2.5 items-center">
                  <input
                    type="password"
                    placeholder="Nhập OTP"
                    name="otp"
                    id="OTP"
                    className="grow"
                    onBlur={formik.handleBlur}
                    onChange={formik.handleChange}
                    value={formik.values.otp}
                  />
                </div>
                {!formik.errors.password &&
                  !formik.errors.passwordConfirm &&
                  formik.errors.otp !=
                    "Otp đã được gửi tới số điện thoại của bạn. Xin hãy kiểm tra tin nhắn." && (
                    <p
                      className={`mt-1 text-sm ${
                        !formik.touched.otp &&
                        formik.errors.otp == "Nhập mã OTP"
                          ? "text-blue-500"
                          : "text-red-500"
                      }`}
                      id="passwordConfirm_warning"
                    >
                      {formik.errors.otp}
                    </p>
                  )}
                <p className="mt-1 text-blue-500 text-sm" id="OTP_confirm">
                  {otpConfirm}
                </p>
              </div>
            </div>

            {/* button đăng ký */}
            <div className="forgotPasswordButton">
              <button
                className="main-border bg-main-blue w-full py-3.5 text-center font-bold text-white"
                type="submit"
              >
                Thay đổi mật khẩu
              </button>
            </div>
          </form>
        )}
        {/* overlay */}
        {openResult && (
          <NextPage
            title="Đổi mật khẩu thành công"
            page="đăng nhập"
            domain="/login"
            state={openResult}
          />
        )}
      </div>
    </div>
  );
}
