import React from "react";
import "../../../../css/RenderCartCartPage.css";
import RenderItemCart from "./RenderItemCart/RenderItemCart";
import couponIcon from "../../../../public/svg-icon/coupon.svg";
import Image from "next/image";

export default function RenderCartCartPage({ arr, setArr }) {
  let renderCart = () => {
    return arr.map((item) => {
      return (
        <RenderItemCart item={item} key={item.id} arr={arr} setArr={setArr} />
      );
    });
  };
  return (
    <div className="renderCartCartPage">
      <div className="container-1 overflow-x-scroll lg:overflow-auto">
        <table className="border-collapse border border-slate-400 w-full">
          <thead>
            <tr className="uppercase">
              <th className="text-left">Sản phẩm</th>
              <th>Giá</th>
              <th>Số lượng</th>
              <th>Tạm tính</th>
              <th>Xóa</th>
            </tr>
          </thead>
          <tbody>
            {renderCart()}
            <tr>
              <td colSpan="5">
                <div className="flex items-center space-x-4 pl-4 py-1 pr-1 rounded-md border border-[#cbcbcb] w-fit">
                  <div className="w-[20px] h-[15px]">
                    <Image alt="..." src={couponIcon} />
                  </div>
                  <input placeholder="Mã khuyến mãi"></input>
                  <button className="bg-main-blue py-3 px-9 rounded text-white font-bold">
                    Áp dụng
                  </button>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  );
}
