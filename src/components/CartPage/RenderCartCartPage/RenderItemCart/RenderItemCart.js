"use client";
import Image from "next/image";
import React from "react";
import "../../../../../css/RenderItemCart.css";
import trashIcon from "../../../../../public/svg-icon/trash.svg";
import { Popconfirm, message } from "antd";
import { setCart } from "@/redux/slice/cartSlice";
import { useDispatch } from "react-redux";

export default function RenderItemCart({ item, arr, setArr }) {
  let dispatch = useDispatch();

  const cancel = (e) => {
    message.error("Đã hủy");
  };
  let handleChangeQuantityInput = (num) => {
    // tạo ra item clone
    let newItem = { ...item };
    if (num !== 0) {
      newItem.quantity = Number(newItem.quantity);
      newItem.quantity = num;
    } else {
      newItem.quantity = "";
    }

    // tạo ra array cart clone
    let cloneArr = [...arr];
    // tìm vị trí sản phẩm
    let index = cloneArr.findIndex((product) => product.id == newItem.id);
    // set cart trên local mới
    if (newItem.quantity == 0 && newItem.quantity != "") {
      handleDelete(newItem.id);
    } else {
      if (newItem.quantity != "") {
        cloneArr[index] = { ...newItem };
        dispatch(setCart(cloneArr));
      }
    }
  };

  let handleChangeQuantityButton = (num) => {
    // tạo ra item clone
    let newItem = { ...item };
    newItem.quantity = num;
    // tạo ra array cart clone
    let cloneArr = [...arr];
    // tìm vị trí sản phẩm
    let index = cloneArr.findIndex((product) => product.id == newItem.id);
    // set cart trên local mới
    if (newItem.quantity == 0) {
      handleDelete(newItem.id);
    } else {
      cloneArr[index] = { ...newItem };
      dispatch(setCart(cloneArr));
    }
  };

  let handleDelete = (id) => {
    let cloneArr = arr.filter((item) => item.id != id);
    dispatch(setCart(cloneArr));
    message.success("Xóa sản phẩm thành công!");
  };

  // const imageParse = item.image && JSON.parse(item.image);

  return (
    <tr className="renderItemCart">
      {/* tên + hình ảnh sản phẩm */}
      <td className="imgName">
        <div className="flex items-center space-x-4">
          <div className="itemImg">
            {/* {imageParse.length > 0 && <Image src={imageParse[0]} alt="..." />} */}
            {item.image != "[ ]" && <img src={item.image} alt="..." />}
          </div>
          <div className="itemName">
            <p>{item.name}</p>
          </div>
        </div>
      </td>
      {/* đơn giá sp */}
      <td>
        <p className="flex justify-center text-main-red font-bold">
          {Number(item.price).toLocaleString("en-US")} VNĐ
        </p>
      </td>
      {/* số lượng sp */}
      <td>
        {/* tăng giảm số lượng */}
        <div className="handleQuantity flex items-center space-x-2.5 justify-center">
          {item.quantity == 1 ? (
            <Popconfirm
              title="Xóa sản phẩm"
              description="Bạn chắc chắn muốn xóa sản phẩm này?"
              onConfirm={() => {
                handleDelete(item.id);
              }}
              okButtonProps={{ danger: true }}
              onCancel={cancel}
              okText="Xóa"
              cancelText="Hủy"
            >
              <button className="changeQuantity">-</button>
            </Popconfirm>
          ) : (
            <button
              className="changeQuantity"
              onClick={() => {
                handleChangeQuantityButton(item.quantity - 1);
                document.getElementById(item.id).value = item.quantity - 1;
              }}
            >
              -
            </button>
          )}

          <input
            className="quantity"
            id={item.id}
            type="text"
            onChange={(event) => {
              const isValidInput = /^\d+$/.test(event.target.value);
              if (isValidInput && event.target.value > 0) {
                if (event.target.validity.valid) {
                  handleChangeQuantityInput(Number(event.target.value));
                } else {
                  handleChangeQuantityInput(1);
                }
              } else {
                event.target.value = "";
                handleChangeQuantityInput("");
              }
              // trường hợp rỗng
            }}
            defaultValue={item.quantity}
          />
          <button
            className="changeQuantity"
            onClick={() => {
              handleChangeQuantityButton(item.quantity + 1);
              document.getElementById(item.id).value = item.quantity + 1;
            }}
          >
            +
          </button>
        </div>
      </td>
      {/* tạm tính */}
      <td>
        <p className="text-main-red font-bold flex justify-center">
          {(item.price * item.quantity).toLocaleString("en-US")} VNĐ
        </p>
      </td>
      {/* xóa */}
      <td>
        <div className="flex justify-center">
          <Popconfirm
            title="Xóa sản phẩm"
            description="Bạn chắc chắn muốn xóa sản phẩm này?"
            onConfirm={() => {
              handleDelete(item.id);
            }}
            okButtonProps={{ danger: true }}
            onCancel={cancel}
            okText="Xóa"
            cancelText="Hủy"
          >
            <button className="trashIcon">
              <Image src={trashIcon} alt="..." />
            </button>
          </Popconfirm>
        </div>
      </td>
    </tr>
  );
}
