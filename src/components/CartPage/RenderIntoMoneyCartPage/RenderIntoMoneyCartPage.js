import React, { useState } from "react";
import "../../../../css/RenderIntoMoneyCartPage.css";
import { getLocalStorage, setLocalStorage } from "@/utils/localStorage";
import { Modal } from "antd";
import { useRouter } from "next/navigation";
import { useSelector } from "react-redux";

export default function RenderIntoMoneyCartPage({
  totalCostOfGoods,
  shipmentFee,
  totalCost,
  productArr,
}) {
  let { settingObj } = useSelector((state) => state.settingSlice);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isModalOpen2, setIsModalOpen2] = useState(false);
  let route = useRouter();
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleOk = () => {
    setLocalStorage("createOrder", true);
    setIsModalOpen(false);
    route.push("/login");
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  const showModal2 = () => {
    setIsModalOpen2(true);
  };
  const handleOk2 = () => {
    setIsModalOpen2(false);
    route.push("/");
  };
  const handleCancel2 = () => {
    setIsModalOpen2(false);
  };

  // CreateOrder
  let createOrder = () => {
    // let myCart = getLocalStorage("cart");
    let token = getLocalStorage("access_token");
    // check đăng nhập
    if (!token) {
      showModal();
    }
    // check giỏ hàng rỗng
    else if (productArr.length == 0) {
      showModal2();
    }
    // các trường hợp còn lại
    else {
      route.push("/cart-payment");
    }
  };
  return (
    <div className="renderIntoMoneyCartPage flex justify-end container-1 pt-10">
      <div className="w-full lg:w-[50%] xl:w-[40%]">
        {/* title */}
        <div className="title font-bold mb-4">
          <h4>Cộng giỏ hàng</h4>
        </div>
        {/* costList*/}
        <div className="costList mb-8">
          {/* Tổng tiên hàng */}
          <div className="totalCostOfGoods grid grid-cols-2">
            <div className="title ">Tổng tiền hàng</div>
            <div className="detail">
              {totalCostOfGoods.toLocaleString("en-US")} VNĐ
            </div>
          </div>
          {/* Phí vận chuyển */}
          {/* <div className="totalCostOfGoods grid grid-cols-2">
            <div className="title ">Phí vận chuyển</div>
            <div className="detail">
              {shipmentFee.toLocaleString("en-US")} VNĐ
            </div>
          </div> */}
          {/* Khuyến mãi */}
          {/* <div className="totalCostOfGoods grid grid-cols-2">
            <div className="title ">Khuyến mãi</div>
            <div className="detail">
              {shipmentFee.toLocaleString("en-US")} VNĐ
            </div>
          </div> */}
          {/* Tổng tiền thanh toán */}
          <div className="totalCostOfGoods grid grid-cols-2">
            <div className="title ">Tổng tiền thanh toán</div>
            <div className="detail font-bold text-main-red">
              {totalCost.toLocaleString("en-US")} VNĐ
            </div>
          </div>
          <div className="text-right">
            {" "}
            <small className="text-red-500">
              * Giá sản phẩm đã bao gồm VAT
            </small>
          </div>
        </div>

        {/* Tiến hành mua hàng */}
        {settingObj.ecommerce && (
          <div className="confirmOrder flex justify-end">
            <button
              className="bg-main-red text-white capitalize w-[300px] lg:w-5/6 py-3.5 flex justify-center main-border"
              onClick={createOrder}
            >
              Tiến hành thanh toán
            </button>
          </div>
        )}

        {/* Modal Login */}
        <Modal
          title="Bạn chưa đăng nhập / Truy cập đã hết hạn"
          open={isModalOpen}
          onOk={handleOk}
          onCancel={handleCancel}
        >
          <p>Đăng nhập để tiếp tục đặt hàng.</p>
          <div className="actionButton pt-6 mt-6 space-x-3 flex justify-end">
            <button
              className="bg-white hover:bg-gray-100 text-gray-800 font-semibold py-2 px-4 border border-gray-400 rounded shadow"
              onClick={handleCancel}
            >
              Để sau
            </button>
            <button
              className="bg-[#003479] hover:bg-[#003F93] text-white font-bold py-2 px-4 rounded inline-flex items-center"
              onClick={handleOk}
            >
              <span>Đăng nhập ngay</span>
            </button>
          </div>
        </Modal>

        {/* Modal Trang chủ */}
        <Modal
          title="Giỏ hàng rỗng, vui lòng thêm sản phẩm vào giỏ hàng"
          open={isModalOpen2}
          onOk={handleOk2}
          onCancel={handleCancel2}
        >
          <p>Quay về trang chủ để tiếp tục đặt hàng.</p>
          <div className="actionButton pt-6 mt-6 space-x-3 flex justify-end">
            <button
              className="bg-white hover:bg-gray-100 text-gray-800 font-semibold py-2 px-4 border border-gray-400 rounded shadow"
              onClick={handleCancel2}
            >
              Để sau
            </button>
            <button
              className="bg-[#003479] hover:bg-[#003F93] text-white font-bold py-2 px-4 rounded inline-flex items-center"
              onClick={handleOk2}
            >
              <span>Mua sắm ngay</span>
            </button>
          </div>
        </Modal>
      </div>
    </div>
  );
}
