import React from "react";
import "../../../../css/DiscriptionProductDetailPage.css";

export default function DiscriptionProductDetailPage({ script }) {
  return (
    <section className="discriptionProductDetailPage container-1">
      <div className="title">
        <h4 className="text-main-blue font-medium inline-block">Mô tả</h4>
      </div>
      <div
        className="content mt-[40px] mb-[80px]"
        id="discriptionProductDetailPage_content"
        dangerouslySetInnerHTML={{ __html: script }}
      ></div>
    </section>
  );
}
