"use client";
import React, { useEffect, useRef } from "react";
import "../../../../css/ImageGalleryProductDetailPage.css";
import { AiOutlineArrowRight, AiOutlineArrowLeft } from "react-icons/ai";
import { Carousel } from "antd";
import { Fancybox } from "@fancyapps/ui";
import "@fancyapps/ui/dist/fancybox/fancybox.css";
import Image from "next/image";
import defaultImg from "../../../assets/img/defaultImg/defaultImg.jpg";

function SampleNextArrow(props) {
  const { onClick } = props;
  return (
    <div className="arrowIcon right-0" onClick={onClick}>
      <AiOutlineArrowRight />
    </div>
  );
}
function SamplePrevArrow(props) {
  const { onClick } = props;
  return (
    <div className="arrowIcon left-0" onClick={onClick}>
      <AiOutlineArrowLeft />
    </div>
  );
}

export default function ImageGalleryProductDetailPage({ gallery }) {
  const ref1 = useRef();
  const ref2 = useRef();

  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    arrows: true,
    swipeToSlide: true,
    centerPadding: 0,
    adaptiveHeight: true,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    responsive: [
      {
        breakpoint: 1023.98,
        settings: {
          slidesToShow: 4,
        },
      },
      {
        breakpoint: 640.98,
        settings: {
          slidesToShow: 3,
        },
      },
    ],
  };

  useEffect(() => {
    // Initialize Fancybox
    Fancybox.bind('[data-fancybox="gallery"]', {
      //
    });
  }, []);

  // render Carousel Thumbnail
  let renderCarouselThumbnail = (carouselNumber) => {
    return gallery.map((item, index) => {
      if (carouselNumber == 2) {
        return (
          <div key={index}>
            {item.image ? (
              <img src={item.image} alt="...." loading="lazy" />
            ) : (
              <Image src={defaultImg} alt="defaultImg" />
            )}
          </div>
        );
      } else {
        return (
          <a
            key={index}
            href={item.image}
            data-fancybox="gallery"
            data-caption="Single image"
          >
            {item.image ? (
              <img src={item.image} alt="...." loading="lazy" />
            ) : (
              <Image src={defaultImg} alt="defaultImg" />
            )}
          </a>
        );
      }
    });
  };

  return (
    <div className="imageGalleryProductDetailPage  my-auto lg:my-0 w-full">
      <Carousel
        ref={ref1}
        infinite={false}
        slidesToShow={1}
        swipeToSlide={false}
      >
        {gallery && renderCarouselThumbnail(1)}
      </Carousel>

      <Carousel
        {...settings}
        ref={ref2}
        swipeToSlide={true}
        focusOnSelect={true}
        centerMode={true}
        afterChange={(current) => {
          ref1.current.goTo(current);
        }}
      >
        {gallery && renderCarouselThumbnail(2)}
      </Carousel>
    </div>
  );
}
