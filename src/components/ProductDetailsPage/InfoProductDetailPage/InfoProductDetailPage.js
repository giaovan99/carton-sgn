import React, { useState } from "react";
import "../../../../css/InfoProductDetailPage.css";
import { useRouter } from "next/navigation";
import { useDispatch, useSelector } from "react-redux";
import { setCart } from "@/redux/slice/cartSlice";
import { message } from "antd";
export default function InfoProductDetailPage({ item }) {
  const { cart } = useSelector((state) => state.cartSlice);
  const router = useRouter();
  const dispatch = useDispatch();
  let { name, price, id, description } = item;
  const [quantity, setQuantity] = useState(1);
  let handleBuyItem = (item) => {
    // add sản phẩm vào giỏ hàng
    handleAddToCart(item);
    // chuyển hướng qua trang giỏ hàng
    router.push("/cart");
  };
  let handleAddToCart = (item) => {
    // Cập nhật state giỏ hàng
    let cloneCart = [...cart];
    let index = cloneCart.findIndex((product) => product.id == item.id);
    if (index == -1) {
      let newItem = { ...item, quantity: quantity };
      cloneCart.push(newItem);
      dispatch(setCart(cloneCart));
    } else {
      let newItem = { ...cloneCart[index] };
      newItem.quantity += quantity;
      cloneCart[index] = { ...newItem };
      dispatch(setCart(cloneCart));
    }
    message.success("Thêm vào giỏ hàng thành công");
  };
  let handleChangeQuantity = (num) => {
    if (num == -1 && quantity == 1) {
      return alert("Số lượng sản phẩm không thể nhỏ hơn 1");
    } else if (quantity >= 1) {
      return setQuantity(quantity + num);
    }
  };
  return (
    <div className="infoProductDetailPage space-y-10">
      {/* thông tin chung về sản phẩm */}
      <div className="item-info space-y-4">
        <div className="title">
          <h2>{name}</h2>
        </div>
        <div className="price">
          <h2 className="text-main-red">
            {price && Number(price)?.toLocaleString("en-US")} VNĐ
          </h2>
        </div>
        <div className="script pt-4">
          <p>{description}</p>
        </div>
      </div>

      {/* button kêu gọi hành động */}
      <div className="button-action pt-6 space-y-10 ">
        {/* tăng giảm số lượng */}
        <div className="handleQuantity flex items-center space-x-2.5">
          <button
            className="changeQuantity"
            onClick={() => {
              handleChangeQuantity(-1);
              if (quantity - 1 > 0) {
                document.getElementById(item.id).value = quantity - 1;
              }
            }}
          >
            -
          </button>
          {/* <p className="quantity ">{quantity}</p> */}
          <input
            className="quantity"
            type="text"
            id={item.id}
            onChange={(event) => {
              const isValidInput = /^-?\d*\.?\d*$/.test(event.target.value);
              if (isValidInput && event.target.value > 0) {
                if (event.target.validity.valid) {
                  setQuantity(Number(event.target.value));
                }
              } else {
                event.target.value = "";
                setQuantity(1);
              }
            }}
            defaultValue={quantity}
          />
          <button
            className="changeQuantity"
            onClick={() => {
              handleChangeQuantity(1);
              document.getElementById(item.id).value = quantity + 1;
            }}
          >
            +
          </button>
        </div>
        {/* Đăt hàng */}
        <div className="hanldeCart space-x-3">
          {/* Mua ngay */}
          <button
            className="buy-item bg-main-blue main-border py-2 px-8 sm:py-3.5 sm:px-16 text-white font-bold"
            onClick={() => {
              handleBuyItem(item);
            }}
          >
            Đặt Ngay
          </button>
          {/* Thêm vào giỏ hàng */}
          <button
            className="add-item main-border border-main-blue py-2 px-4 sm:py-3.5 sm:px-6 text-main-blue font-bold"
            onClick={() => {
              handleAddToCart(item);
            }}
          >
            <div className="bag-icon">Thêm Vào Giỏ Hàng</div>
          </button>
        </div>
      </div>
    </div>
  );
}
