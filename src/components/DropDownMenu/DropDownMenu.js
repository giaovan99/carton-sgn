"use client";
import React from "react";
import ItemDropDownMenu from "./ItemDropDownMenu/ItemDropDownMenu";
import "../../../css/dropDownMenu.css";
import { useSelector } from "react-redux";

export default function DropDownMenu({ showNavbar }) {
  const { arrProductType } = useSelector((state) => state.productSlice);

  let renderItem = () => {
    return arrProductType.map((item, index) => {
      return (
        <ItemDropDownMenu
          type={item.name}
          key={index}
          id={item.id}
          showNavbar={showNavbar}
        />
      );
    });
  };

  return (
    <div className=" bg-white text-black " id="dropDownMenu">
      {renderItem()}
    </div>
  );
}
