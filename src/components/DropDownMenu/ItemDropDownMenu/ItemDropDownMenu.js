import React from "react";
import Image from "next/image";
import circleIcon from "../../../../public/dropDownMenu/circle.png";
import "../../../../css/ItemDropDownMenu.css";
import Link from "next/link";
import { useDispatch } from "react-redux";
import { setDefinePageTitle } from "@/redux/slice/productSlice";

export default function ItemDropDownMenu({ type, id, showNavbar = null }) {
  const dispatch = useDispatch();
  let setUpNextPage = () => {
    dispatch(setDefinePageTitle({ title: type, id }));
    if (showNavbar) {
      showNavbar();
    }
  };
  return (
    <Link
      className="product-item space-x-1.5"
      href={`/product-list?categoryId=${id}`}
      onClick={setUpNextPage}
    >
      <span className="circleIcon">
        <Image src={circleIcon} alt="..." />
      </span>
      <span>{type}</span>
    </Link>
  );
}
