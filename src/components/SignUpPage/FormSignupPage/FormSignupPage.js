"use client";
import React, { useEffect, useState } from "react";
import ReCAPTCHA from "react-google-recaptcha";
import "../../../../css/FormSignupPage.css";
import Image from "next/image";
import phoneIcon from "../../../../public/svg-icon/call.svg";
import passIcon from "../../../../public/svg-icon/lock.svg";
import passIcon2 from "../../../../public/svg-icon/password-reset.png";
import { setDefinePageTitle } from "@/redux/slice/productSlice";
import { useDispatch } from "react-redux";
import { userService } from "@/services/customerService";
import { useFormik } from "formik";
import * as yup from "yup";
import CountDown from "@/components/CountDown/CountDown";
import NextPage from "@/components/NextPage/NextPage";

export default function FormSignupPage() {
  const [activeSubmit, setActiveSubmit] = useState(false);
  const [openCountDown, setOpenCountDown] = useState(false);
  const [openResult, setOpenResult] = useState(false);
  const [captcha, setCaptcha] = useState();
  const formik = useFormik({
    initialValues: {
      phone: "",
      password: "",
      passwordConfirm: "",
      otp: "",
    },
    validationSchema: yup.object({
      phone: yup
        .string()
        .required("Nhập số điện thoại")
        .matches(
          /^(?!.*[^\d+])(84|0[35789])(\d{8})\b/,
          "Số điện thoại không hợp lệ"
        ),
      password: yup
        .string()
        .required("Nhập mật khẩu")
        .matches(
          /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/,
          "Mật khẩu yêu cầu ít nhất 8 ký tự (ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)"
        ),
      passwordConfirm: yup
        .string()
        .required("Xác nhận lại mật khẩu")
        .oneOf([yup.ref("password"), null], "Mật khẩu chưa trùng khớp"),
      // otp: yup.string().required("Lấy và nhập OTP"),
    }),
    onSubmit: async (values) => {
      if (captcha) {
        document.getElementById("reCAPTCHA_warning").innerHTML = "";
        // const active = await checkOTP({ otp: values.otp, phone: values.phone });
        let active = true;
        if (active) {
          apiCreateCustomer(values);
        }
      } else {
        document.getElementById("reCAPTCHA_warning").innerHTML =
          "Xác minh bạn không phải người máy.";
      }
    },
  });
  const dispatch = useDispatch();

  // checkOTP
  let checkOTP = async (data) => {
    let res = await userService.checkOTP(data);
    if (res.data.success) {
      document.getElementById("OTP_warning").innerText = "";
      return true;
    } else {
      document.getElementById("OTP_warning").innerText = res.data.message;
      document.getElementById("otp").focus();
      return false;
    }
  };

  let abledGetOTP = () => {
    setOpenCountDown(false);
    // formik.setFieldError('otp', '');
    document.getElementById("OTP_check").disabled = false;
    // document.getElementById('OTP_warning').innerText = '';
  };

  // getOTP
  let getOTP = async () => {
    if (
      !formik.errors.phone &&
      !formik.errors.password &&
      !formik.errors.passwordConfirm &&
      formik.values.phone != "" &&
      formik.values.password != "" &&
      formik.values.passwordConfirm != ""
    ) {
      // vô get OTP tắt luôn warning
      formik.setFieldError("otp", "");
      let res = await userService.getOTP({ phone: formik.values.phone });
      if (res.data.success) {
        formik.setFieldError("otp", res.data.message);
        setOpenCountDown(true);

        // Khúc này đang cho fix cứng đoạn text -> res.data.message
        document.getElementById("OTP_warning_api").innerHTML =
          "Mã Otp đã được gửi tới Zalo, xin hãy kiểm tra tin nhắn";
        document.getElementById("otp").focus();
        // tới đây oke
        document.getElementById("OTP_check").disabled = true;
        setTimeout(abledGetOTP, 180000);
      } else {
        // xuóng đây ẩn setFieldError của otp
        // formik.setErrors({});
        // formik.setFieldValue('phone', formik.values.phone, false);
        formik.touched.otp = true;
        document.getElementById("phone").focus();
        document.getElementById("phone_exist").innerText =
          res.data.error.phone[0];
        document.getElementById("OTP_warning").innerText = "";
      }
    }
  };

  // function
  useEffect(() => {
    dispatch(setDefinePageTitle({ title: "Đăng ký", id: -1 }));
  }, []);

  // Focus vào input chưa hợp lệ
  let focusCursor = (id, idValid, active) => {
    if (active) {
      document.getElementById(id).focus();
    }

    //  sẽ chia 2 TH check, chưa submit và đã submit bằng state activeSubmit, khi submit xong cho nó true rồi bật ngược về false
    if (
      activeSubmit &&
      (formik.errors["phone"] !== "Nhập số điện thoại" ||
        formik.errors["password"] !== "Nhập mật khẩu" ||
        formik.errors["passwordConfirm"] !== "Xác nhận lại mật khẩu" ||
        formik.errors["otp"] !== "Lấy và nhập OTP")
    ) {
      return (
        <p className="mt-1 text-red-500 text-sm" id={idValid}>
          {formik.errors[id]}
        </p>
      );
    } else if (
      !activeSubmit &&
      (formik.errors[id] == "Nhập số điện thoại" ||
        formik.errors[id] == "Nhập mật khẩu" ||
        formik.errors[id] == "Xác nhận lại mật khẩu" ||
        formik.errors[id] == "Lấy và nhập OTP")
    ) {
      return (
        <p className="mt-1 text-main-blue-2 text-sm" id={idValid}>
          {formik.errors[id]}
        </p>
      );
    } else {
      return (
        <p className="mt-1 text-red-500 text-sm" id={idValid}>
          {formik.errors[id]}
        </p>
      );
    }
  };

  // apiCreateCustomer
  let apiCreateCustomer = async (data) => {
    let res = await userService.create(data).catch((err) => {
      return false;
    });
    // console.log(res);
    if (res.data.success) {
      setOpenResult(true);
      document.getElementById("phone_exist").innerText = "";
      // router.push("/login");
      return true;
    } else {
      // formik.setFieldValue("phone", false);
      // document.getElementById.innerHTML = formik.setFieldError(
      //   "phone",
      //   res.data.error.phone[0]
      // );
      document.getElementById("phone_exist").innerText =
        res.data.error.phone[0];
      // document.getElementById("phone").focus();
      // formik.setFieldError("phone", res.data.error.phone[0]);
      // console.log(formik);
      return false;
    }
  };

  return (
    <div className="formSignupPage">
      <div className="container-1 tb-padding-2 relative">
        <form
          className="formLG flex flex-col justify-between"
          onSubmit={(event) => {
            event.preventDefault();
            setActiveSubmit(true);
            for (let item in formik.errors) {
              document.getElementById(item).focus();
              return;
            }
            formik.handleSubmit();
          }}
        >
          {/* title */}
          <div className="title text-center font-bold">
            <h4>Đăng ký tài khoản</h4>
          </div>

          {/* form */}
          <div className="userInfo space-y-4 mt-4">
            {/* input Tài khoản */}
            <div>
              <div className="inputUserInfo flex space-x-2.5 items-center">
                <label htmlFor="phone">
                  <Image
                    src={phoneIcon}
                    alt="..."
                    className="iconInputUserInfo"
                  />
                </label>
                <input
                  type="text"
                  placeholder="Số điện thoại"
                  name="phone"
                  id="phone"
                  className="grow"
                  value={formik.values.phone}
                  onChange={(event) => {
                    formik.handleChange(event);
                    document.getElementById("phone_exist").innerText = "";
                  }}
                  onFocus={() => {
                    formik.setFieldTouched("phone", true);
                  }}
                />
              </div>
              {formik.errors.phone &&
                focusCursor("phone", "phone_warning", true)}
              <p className="mt-1 text-red-500 text-sm " id="phone_exist"></p>
            </div>
            {/* input Mật khẩu */}
            <div>
              <div className="inputUserInfo flex space-x-2.5 items-center">
                <label htmlFor="password">
                  <Image
                    src={passIcon}
                    alt="..."
                    className="iconInputUserInfo"
                  />
                </label>
                <input
                  type="password"
                  placeholder="Mật khẩu"
                  name="password"
                  id="password"
                  className="grow"
                  value={formik.values.password}
                  onChange={formik.handleChange}
                  onFocus={() => {
                    formik.setFieldTouched("password", true);
                  }}
                />
              </div>
              {!formik.errors.phone &&
                formik.errors.password &&
                focusCursor("password", "password_warning", true)}
            </div>
            {/* input Nhập lại Mật khẩu */}
            <div>
              <div className="inputUserInfo flex space-x-2.5 items-center">
                <label htmlFor="password">
                  <Image
                    src={passIcon2}
                    alt="..."
                    className="iconInputUserInfo"
                  />
                </label>
                <input
                  type="password"
                  placeholder="Xác nhận lại mật khẩu"
                  name="passwordConfirm"
                  id="passwordConfirm"
                  className="grow"
                  value={formik.values.passwordConfirm}
                  onChange={formik.handleChange}
                  onFocus={() => {
                    formik.setFieldTouched("passwordConfirm", true);
                  }}
                />
              </div>
              {!formik.errors.phone &&
                !formik.errors.password &&
                formik.errors.passwordConfirm &&
                focusCursor(
                  "passwordConfirm",
                  "passwordConfirm_warning",
                  false
                )}
            </div>
            {/* input OTP */}
            {/* <div>
              <div className="inputUserInfo  flex space-x-2.5 items-center">
                <input
                  type="text"
                  placeholder="Nhập mã OTP"
                  name="otp"
                  id="otp"
                  className="grow"
                  onChange={formik.handleChange}
                  onFocus={() => {
                    formik.setFieldTouched("otp", true);
                  }}
                />
                <button
                  className="text-main-blue  underline hover:font-medium duration-500 disabled:text-gray-600 disabled:cursor-not-allowed"
                  onClick={getOTP}
                  id="OTP_check"
                >
                  Lấy mã
                </button>
              </div>

              {!formik.errors.phone &&
              !formik.errors.password &&
              !formik.errors.passwordConfirm &&
              formik.errors.otp &&
              !openCountDown ? (
                focusCursor("otp", "OTP_warning", false)
              ) : (
                <p className="mt-1 text-red-500 text-sm" id="OTP_warning"></p>
              )}

              <p className="mt-1 text-red-500 text-sm">
                <span id="OTP_warning_api"></span>
              </p>

              {openCountDown && (
                <span className="mt-1 text-red-500 text-sm">
                  Thử lại sau{" "}
                  <CountDown
                    openCountDown={openCountDown}
                    periodTime={180000}
                  />{" "}
                  phút
                </span>
              )}
            </div> */}
          </div>

          <div>
            {/* reCAPTCHA */}
            <div className="reCAPTCHA mt-4">
              {/* <Image src={recaptchaImg} alt="..." /> */}
              <ReCAPTCHA
                // sitekey={process.env.NEXT_PUBLIC_RECAPTCHA_SITE_KEY}
                sitekey={"6LeZOM8pAAAAAH8UlE1lSEZliUvyVU2NA9pEdi0i"}
                onChange={setCaptcha}
              />
            </div>
            <p
              className="mt-1 text-red-500 text-sm hidden"
              id="reCAPTCHA_warning"
            ></p>
          </div>

          {/* button đăng ký */}
          <div className="signupButton">
            <button
              className="main-border bg-main-blue w-full py-3.5 text-center font-bold text-white"
              type="submit"
            >
              Đăng Ký
            </button>
          </div>
        </form>

        {/* overlay */}
        {openResult && (
          <NextPage
            title="Đăng ký thành công"
            page="đăng nhập"
            domain="/login"
            state={openResult}
          />
        )}
      </div>
    </div>
  );
}
