import React from "react";
import ContactInfo from "./ContactInfo";
import ContactForm from "./ContactForm";

export default function ContactDetail() {
  return (
    <div className="contactDetail">
      <div className="container-1 tb-padding-2 space-y-11">
        {/* title */}
        <h2 className="uppercase ">
          CÔNG TY TNHH ĐẦU TƯ THƯƠNG MẠI{" "}
          <span className="text-main-blue">SÀI GÒN NGUYỄN</span>
        </h2>
        {/* contactDetail */}
        <div className=" grid grid-cols-1 lg:grid-cols-2 gap-6 lg:gap-12">
          {/* leftside */}
          <ContactInfo />

          {/* rightside */}
          <ContactForm />
        </div>
      </div>
    </div>
  );
}
