import React from "react";
import { useSelector } from "react-redux";
import { pinIcon } from "./../../../../public/svg-icon/pin.svg";

export default function ContactInfo() {
  let { settingObj } = useSelector((state) => state.settingSlice);
  return (
    <div className="ContactInfo space-y-6">
      {/* Xưởng */}

      <div
        dangerouslySetInnerHTML={{ __html: settingObj.address_contact }}
        className="space-y-2 xl:space-y-4"
      ></div>
      {/* contact */}
      <div className="space-y-4">
        <ul className="list-none">
          <li>
            Điện thoại:{" "}
            <a href={`tel:${settingObj.hotline}`} className="text-main-blue">
              {settingObj.hotline}
            </a>
          </li>
          <li>
            Zalo:{" "}
            <a href={`tel:${settingObj.hotline}`} className="text-main-blue">
              {settingObj.hotline}
            </a>
          </li>
          <li>
            Email:{" "}
            <a href={`mailto:${settingObj.email}`} className="text-main-blue">
              {settingObj.email}
            </a>
          </li>
          <li>
            Website:{" "}
            <a href="/" className="text-main-blue">
              {settingObj.site}
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
}
