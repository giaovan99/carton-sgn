import React, { useState } from "react";
import "../../../../css/ContactForm.css";
import { useFormik } from "formik";
import * as yup from "yup";
import { pageService } from "@/services/page";

export default function ContactForm() {
  const [activeSubmit, setActiveSubmit] = useState(false);
  const [sendConfirm, setSendConfirm] = useState(false);

  const formik = useFormik({
    initialValues: {
      name: "",
      organization: "",
      address: "",
      phone: "",
      email: "",
      message: "",
    },
    validationSchema: yup.object({
      name: yup.string().required("Họ và Tên không để trống"),
      organization: yup.string(),
      address: yup.string(),
      phone: yup
        .string()
        .required("Số điện thoại không để trống")
        .matches(
          /^(?!.*[^\d+])(84|0[35789])(\d{8})\b/,
          "Số điện thoại không hợp lệ"
        ),
      email: yup.string(),
      message: yup.string(),
    }),
    onSubmit: async (values, { resetForm }) => {
      // gửi API
      let res = await pageService.sendContact({
        name: values.name,
        organization: values.organization,
        address: values.address,
        phone: values.phone,
        email: values.email,
        message: values.message,
      });
      if (res.data.success) {
        setSendConfirm(true);
        resetForm();
      }
    },
  });

  let focusCursor = (id, idValid, active) => {
    if (active) {
      document.getElementById(id).focus();
    }

    //  sẽ chia 2 TH check, chưa submit và đã submit bằng state activeSubmit, khi submit xong cho nó true rồi bật ngược về false
    if (
      activeSubmit &&
      (formik.errors["phone"] !== "Số điện thoại không để trống" ||
        formik.errors["name"] !== "Họ và Tên không để trống")
    ) {
      return (
        <p className="mt-1 text-red-500 text-sm" id={idValid}>
          {formik.errors[id]}
        </p>
      );
    } else if (
      !activeSubmit &&
      (formik.errors[id] == "Số điện thoại không để trống" ||
        formik.errors[id] == "Họ và Tên không để trống")
    ) {
      return (
        <p className="mt-1 text-main-blue-2 text-sm" id={idValid}>
          {formik.errors[id]}
        </p>
      );
    } else {
      return (
        <p className="mt-1 text-red-500 text-sm" id={idValid}>
          {formik.errors[id]}
        </p>
      );
    }
  };
  return (
    <div className="contactForm">
      <form
        className="space-y-1 lg:space-y-3.5"
        onSubmit={(event) => {
          event.preventDefault();
          setActiveSubmit(true);
          for (let item in formik.errors) {
            document.getElementById(item).focus();
            return;
          }
          formik.handleSubmit();
        }}
      >
        {/* Họ tên */}
        <div className="formInfo space-y-1.5">
          <label htmlFor="name">
            Họ và Tên<span className="text-main-red">*</span>
          </label>
          <br />
          <input
            type="text"
            id="name"
            name="name"
            value={formik.values.name}
            onChange={formik.handleChange}
            onFocus={() => {
              formik.setFieldTouched("name", true);
            }}
          />
          {formik.errors.name && focusCursor("name", "name_warning", true)}{" "}
        </div>
        {/* Đơn vị */}
        <div className="formInfo space-y-1.5">
          <label htmlFor="organization">Đơn vị</label>
          <br />
          <input
            type="text"
            id="organization"
            name="organization"
            value={formik.values.organization}
            onChange={formik.handleChange}
          />
        </div>
        {/* Địa chỉ */}
        <div className="formInfo space-y-1.5">
          <label htmlFor="address">Địa chỉ</label>
          <br />
          <input
            type="text"
            id="address"
            name="address"
            value={formik.values.address}
            onChange={formik.handleChange}
          />
        </div>
        {/* Sdt & mail */}
        <div className="grid lg:grid-cols-2 gap-4">
          {/* Số điện thoại */}
          <div className="formInfo space-y-1.5">
            <label htmlFor="phone">
              Số điện thoại<span className="text-main-red">*</span>
            </label>
            <br />
            <input
              type="text"
              id="phone"
              name="phone"
              value={formik.values.phone}
              onChange={formik.handleChange}
              onFocus={() => {
                formik.setFieldTouched("phone", true);
              }}
            />
            {!formik.errors.name &&
              formik.errors.phone &&
              focusCursor("phone", "phone_warning", false)}{" "}
          </div>
          {/* Email */}
          <div className="formInfo space-y-1.5">
            <label htmlFor="email">Email</label>
            <br />
            <input
              type="text"
              id="email"
              name="email"
              value={formik.values.email}
              onChange={formik.handleChange}
            />
          </div>
        </div>
        {/* Tin nhắn */}
        <div className="formInfo space-y-1.5">
          <label htmlFor="message">Tin nhắn</label>
          <br />
          <textarea
            id="message"
            name="message"
            value={formik.values.message}
            onChange={formik.handleChange}
          />
        </div>
        {/* button gửi */}
        <div>
          {/* Confirm Send Contact */}
          {sendConfirm && (
            <p className="sendConfirm text-main-blue">
              Yêu cầu liên hệ đã được gửi đi thành công
            </p>
          )}
          <button
            className="bg-main-blue py-3.5 px-16 text-white font-bold rounded-[26px] mt-6"
            type="submit"
          >
            Gửi
          </button>
        </div>
      </form>
    </div>
  );
}
