"use client";
import React from "react";
import RenderProductItem from "./RenderProductItem/RenderProductItem";
import "../../../css/RenderProductList.css";
import { Carousel } from "antd";
import { IoIosArrowForward, IoIosArrowBack } from "react-icons/io";
import Image from "next/image";
import bottomLine from "../../../public/renderProductList/bottomLine.png";

let SampleNextArrow = (props) => {
  const { style, onClick } = props;
  return (
    <div
      className="py-[20px] px-1 rounded-md border-spacing-2 rightArrow"
      style={{
        ...style,
      }}
      onClick={onClick}
    >
      <IoIosArrowForward />
    </div>
  );
};
let SamplePrevArrow = (props) => {
  const { style, onClick } = props;
  return (
    <div
      className="py-[20px] px-1 rounded-md border-spacing-2 leftArrow"
      style={{
        ...style,
      }}
      onClick={onClick}
    >
      <IoIosArrowBack />
    </div>
  );
};

export default function RenderProductList({ title, arr }) {
  const settings = {
    dots: false,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: true,
    infinite: true,
    adaptiveHeight: true,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    responsive: [
      {
        breakpoint: 1023.98,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 767.98,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          autoplay: true,
          autoplaySpeed: 2000,
          dots: true,
          infinite: true,
        },
      },
    ],
  };
  let renderCarouselListItem = () => {
    return arr.map((item) => {
      return <RenderProductItem item={item} key={item.id} />;
    });
  };
  return (
    <div className="renderProductList container-1 tb-padding">
      <h2 className="mb-5 lg:mb-10 uppercase text-center flex justify-center flex-col text-main-blue items-center">
        {title}
        <br />
        <Image
          src={bottomLine}
          alt="bottomLine"
          width={148.16}
          className="mt-3"
        ></Image>
      </h2>
      <div className="renderCarouselListItem">
        <Carousel {...settings}>{renderCarouselListItem()}</Carousel>
      </div>
    </div>
  );
}
