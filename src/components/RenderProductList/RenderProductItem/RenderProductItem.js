"use client";
import React from "react";
import "../../../../css/renderProductItem.css";
import Image from "next/image";
import bagIcon from "../../../../public/renderProductList/bag.svg";
import { message } from "antd";
import { useDispatch, useSelector } from "react-redux";
import defautImg from "../../../../public/defautImg/defautImg.jpg";
import {
  setDefinePageTitle,
  setProductDetail,
} from "@/redux/slice/productSlice";
import { useRouter } from "next/navigation";
import { setCart } from "@/redux/slice/cartSlice";
export default function RenderProductItem({ item }) {
  const { cart } = useSelector((state) => state.cartSlice);
  const router = useRouter();
  const dispatch = useDispatch();
  let handleBuyItem = (item) => {
    // add sản phẩm vào giỏ hàng
    handleAddToCart(item);
    // chuyển hướng qua trang giỏ hàng
    router.push("/cart");
  };
  let handleAddToCart = (item) => {
    let cloneCart = [...cart];
    let index = cloneCart.findIndex((product) => product.id == item.id);
    if (index == -1) {
      let newItem = { ...item, quantity: 1 };
      cloneCart.push(newItem);
      dispatch(setCart(cloneCart));
    } else {
      let newItem = { ...cloneCart[index] };
      newItem.quantity++;
      cloneCart[index] = { ...newItem };
      dispatch(setCart(cloneCart));
    }
    message.success("Thêm vào giỏ hàng thành công");
  };
  let handleDetail = (item) => {
    dispatch(setDefinePageTitle({ title: item.name, id: item.id }));
    dispatch(setProductDetail(item));
    router.push(`/product-details/${item.slug}`);
  };
  // const imageParse = item.image && JSON.parse(item.image);

  return (
    <div className=" py-3 px-2 lg:px-8 md:p-3 md:py-5 h-full border-b-[#ECECEC] border-b">
      <div className="renderProductItem p-[10px] space-y-3 overflow-hidden">
        <div
          className="item-info space-y-4 cursor-pointer"
          onClick={() => {
            handleDetail(item);
          }}
        >
          {/* Hình ảnh */}
          <div className="item-img overflow-hidden">
            {/* {imageParse.length > 0 && <Image src={imageParse[0]} alt="..." />} */}
            {item.image ? (
              <img src={item.image} alt="..." loading="lazy" />
            ) : (
              <Image src={defautImg} alt="..." />
            )}
          </div>
          {/* Tên sp */}
          <div className="title text-center ">
            <p className="line-clamp-2 capitalize">{item.name}</p>
          </div>
          {/* Giá */}
          <div className="price text-center text-main-red font-bold">
            <p>{Number(item.price)?.toLocaleString("en-US")} VNĐ</p>
          </div>
        </div>
        {/* Button hành động */}
        <div className="action-cart space-y-2">
          <div className="flex justify-center space-x-2 items-center">
            {/* Mua ngay */}
            <button
              className="buy-item bg-main-blue main-border h-[30px] px-4 xl:px-8 text-white"
              onClick={() => {
                handleBuyItem(item);
              }}
            >
              Đặt Ngay
            </button>
            {/* Thêm vào giỏ hàng */}
            <button
              className="add-item"
              onClick={() => {
                handleAddToCart(item);
              }}
            >
              <div className="bag-icon">
                <Image src={bagIcon} alt="..." />
              </div>
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
