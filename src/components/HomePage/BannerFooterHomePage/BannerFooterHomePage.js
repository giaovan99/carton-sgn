import React from "react";
import "../../../../css/BannerFooterHomePage.css";
import Image from "next/image";
import footerBanner from "../../../../public/homePage/footerBanner.jpg";

export default function BannerFooterHomePage() {
  return (
    <section className="bannerFooterHomePage ">
      <div className="top bg-main-gray h-2/4"></div>
      <div className="bot bg-main-blue h-2/4"></div>
      <div className="banner-img container-1 relative">
        <Image src={footerBanner} alt="..." />
      </div>
    </section>
  );
}
