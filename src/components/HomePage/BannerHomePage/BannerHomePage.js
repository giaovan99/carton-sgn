"use client";
import DropDownMenu from "@/components/DropDownMenu/DropDownMenu";
import React from "react";
import "../../../../css/BannerHomePage.css";
import Image from "next/image";
import carouselBanner from "../../../../public/homePage/banner.jpg";
import carouselBanner2 from "../../../../public/homePage/banner02.png";

import { Carousel } from "antd";

export default function BannerHomePage() {
  return (
    <section
      className="bannerHomePage flex container-1 overflow-hidden"
      style={{ height: "initial" }}
    >
      {/* MenuDropDown */}
      <div className="left-side hidden lg:block dropDownMenuHomePage">
        <DropDownMenu />
      </div>
      {/* Banner */}
      <div
        className="right-side grow lg:pl-6 mt-[24px] grid grid-cols-1"
        style={{ height: "inherit" }}
      >
        <div className=" h-full xl:h-[370px] flex flex-col justify-start">
          <Carousel autoplay infinite adaptiveHeight className="h-full">
            <div style={{ height: "100%" }}>
              <Image src={carouselBanner} alt="..." />
            </div>
            <div style={{ height: "100%" }}>
              <Image src={carouselBanner2} alt="..." />
            </div>
          </Carousel>
        </div>
      </div>
    </section>
  );
}
