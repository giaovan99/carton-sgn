import React from "react";
import "../../../../css/NewsHomePage.css";
import Image from "next/image";
import NewsItem from "./NewsItem/NewsItem";
import bottomLine from "../../../../public/renderProductList/bottomLine.png";
import Link from "next/link";
export default function NewsHomePage({ newsArr }) {
  // const newsArr = [
  //   {
  //     id: 1,
  //     img: newsImg1,
  //     title: "Thanh nẹp giấy là gì ? Tại sao phải sử dụng thanh nẹp giấy?",
  //     date: "15-02-2022",
  //     disc: "Thanh nẹp giấy là một công cụ dùng để nẹp cạnh, nẹp góc các sản phẩm và được sử dụng phổ biến trong các ngành đóng gói, xuất khẩu hàng hóa.",
  //   },
  //   {
  //     id: 2,
  //     img: newsImg2,
  //     title: "TÚI BÓNG KHÍ LÀ GÌ? ĐẶC TÍNH CỦA LOẠI SẢN PHẨM NÀY LÀ GÌ?",
  //     date: "15-02-2022",
  //     disc: "Ngày nay khi mà các sản phẩm cao cấp đang tràn ngập trên thị trường thì việc bảo vệ tốt nhất cho dòng sản phẩm này cũng là điều mà tất cả các nhà kinh Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui inventore sapiente ducimus accusamus provident delectus deleniti dolor. Architecto tenetur veritatis atque recusandae! Quasi, corrupti repellat? Sint dolorum nihil neque maxime.",
  //   },
  //   {
  //     id: 3,
  //     img: newsImg3,
  //     title: "THÙNG CARTON VÀ QUY TRÌNH SẢN XUẤT THÙNG CARTON",
  //     date: "15-02-2022",
  //     disc: "Thùng carton đang ngày càng chiếm ưu thế so với các bao bì nilong vì tính tiện dụng và mức độ thân thiện với môi trường.Không những vậy, đây còn là Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui inventore sapiente ducimus accusamus provident delectus deleniti dolor. Architecto tenetur veritatis atque recusandae! Quasi, corrupti repellat? Sint dolorum nihil neque maxime.",
  //   },
  // ];
  let renderListNews = () => {
    return newsArr.map((item, index) => {
      return <NewsItem item={item} key={index} />;
    });
  };
  return (
    <section className="newsHomePage bg-main-gray ">
      <div className="container-1 tb-padding space-y-10">
        {/* title */}
        <h2 className="uppercase text-center flex justify-center flex-col text-main-blue items-center">
          Tin tức
          <br />
          <Image
            src={bottomLine}
            alt="bottomLine"
            width={148.16}
            className="mt-3"
          ></Image>
        </h2>
        {/* render list news */}
        <div className="renderListNews grid grid-cols-1 lg:grid-cols-3 gap-8">
          {renderListNews()}
        </div>
        {/* button xem thêm */}
        <div className="xem-them flex justify-center">
          <Link
            href="/news-list"
            className="main-border bg-main-blue text-white py-2 px-12 sm:py-3.5 sm:px-20"
          >
            Xem Thêm
          </Link>
        </div>
      </div>
    </section>
  );
}
