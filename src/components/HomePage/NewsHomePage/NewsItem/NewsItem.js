import React from "react";
import "../../../../../css/NewsItem.css";
import Image from "next/image";
import Link from "next/link";
import moment from "moment";

export default function NewsItem({ item }) {
  return (
    <Link
      className="newsItem lg:space-y-6 flex items-center lg:justify-between space-x-3 lg:space-x-0 lg:block"
      href={`/news-details/` + item.slug}
    >
      <div className="item-img">
        <img src={item.image} alt="..." loading="lazy" />
      </div>
      <div className="item-info space-y-2 grow">
        <div className="date">
          <p>{moment(item.created_at).format("DD-MM-YYYY")}</p>
        </div>
        <div className="title">
          <p className="p2 font-bold uppercase line-clamp-2 min-h-[44px]">
            {item.name}
          </p>
        </div>
        <div className="disc">
          <p
            className="p2 line-clamp-4"
            dangerouslySetInnerHTML={{ __html: item.description }}
          ></p>
        </div>
      </div>
    </Link>
  );
}
