"use client";
import React, { useEffect, useState } from "react";
import "../../../css/DetailAccountPage.css";
import Image from "next/image";
import circleIcon from "../../../public/dropDownMenu/circle.png";
import Link from "next/link";
import { Modal, message } from "antd";
import { getLocalStorage } from "@/utils/localStorage";
import { userService } from "@/services/customerService";
import { useRouter } from "next/navigation";
import { Select } from "antd";
import { orderService } from "@/services/orderService";
import { useFormik } from "formik";

export default function DetailAccountPage() {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [userInfo, setUserInfo] = useState();
  const [district, setDistrict] = useState();
  // const [districtChoosen, setProvince] = useState();

  const formik = useFormik({
    initialValues: {
      full_name: userInfo?.full_name,
      address: userInfo?.address,
      shipping_address: userInfo?.shipping_address,
      email: userInfo?.email,
      shipping_province: userInfo?.shipping_province,
      shipping_district: userInfo?.shipping_district,
    },
    onSubmit: async (values) => {
      if (values.full_name == "" || !values.full_name) {
        values.full_name = userInfo.full_name;
      }
      if (values.address == "" || !values.address) {
        values.address = userInfo.address;
      }
      if (values.shipping_address == "" || !values.shipping_address) {
        values.shipping_address = userInfo.shipping_address;
      }
      if (values.email == "" || !values.email) {
        values.email = userInfo.email;
      }
      if (values.shipping_province == "Khác") {
        values.shipping_district = "";
      }
      let res = await userService
        .updateAccountInfo({
          token: getLocalStorage("access_token").token,
          full_name: values.full_name,
          address: values.address,
          shipping_address: values.shipping_address,
          email: values.email,
          shipping_province: values.shipping_province,
          shipping_district: values.shipping_district,
        })
        .catch((err) => {
          console.log(err);
        });
      if (res.data.code == 200) {
        message.success("Cập nhật thông tin cá nhân thành công");
        handleOk();
        getUserInfo();
      } else if (res.data.code == 201) {
        message.error(
          "Cập nhật thông tin thất bại. Hết hạn truy cập. Vui lòng đăng nhập lại"
        );
      }
    },
  });

  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    formik.setValues(userInfo);
    setIsModalOpen(false);
  };

  let route = useRouter();
  let now = new Date();

  // lấy thông tin người dùng
  let getUserInfo = async () => {
    let token = getLocalStorage("access_token");
    if (!token || now.getTime() > token.expiry) {
      message.error("Đăng nhập hết hạn, thử lại.");
      route.push("/");
    } else {
      let res = await userService.getInfo(token.token).catch((err) => {
        console.log(err);
      });
      if (res.status == 200) {
        console.log(res.data);
        setUserInfo(res.data);
        // setProvince(res.data.shipping_province);
        formik.setValues(res.data);
      }
    }
  };

  // lấy danh sách quận
  let getDistrict = async () => {
    let res = await orderService.district().catch((err) => {
      console.log(err);
    });
    if (res.data.success == true) {
      let newArr = res.data.data.map((item) => {
        return {
          value: item.id,
          label: `${item.type} ${item.name}`,
        };
      });
      setDistrict(newArr);
    }
  };

  useEffect(() => {
    getUserInfo();
    getDistrict();
  }, []);

  return (
    <div className="DetailAccountPage">
      <div className="container-1 tb-padding-3 flex">
        {/* leftSide - MenuDropDown*/}
        <div className="left-side hidden lg:block dropDownMenuHomePage basic-1/5">
          <div className=" bg-white text-black " id="dropDownMenu">
            <Link
              className="product-item space-x-1.5 cursor-pointer"
              href="/account"
            >
              <span className="circleIcon">
                <Image src={circleIcon} alt="..." />
              </span>
              <span>Thông tin cá nhân</span>
            </Link>
            <Link
              className="product-item space-x-1.5 cursor-pointer"
              href="/cart"
            >
              <span className="circleIcon">
                <Image src={circleIcon} alt="..." />
              </span>
              <span>Giỏ hàng của tôi</span>
            </Link>
            <Link
              className="product-item space-x-1.5 cursor-pointer"
              href="/purchase-history"
            >
              <span className="circleIcon">
                <Image src={circleIcon} alt="..." />
              </span>
              <span>Lịch sử mua hàng</span>
            </Link>
          </div>
        </div>

        {/* rightSide */}
        <div className="right-side basis-full lg:basis-4/5 lg:pl-8">
          {/* Thông tin cá nhân */}
          <div className="tableInfo border-b-2 border-[#ECECEC] lg:py-6">
            <table>
              {/* Họ tên */}
              <tr>
                <td className="title">Họ và Tên</td>
                <td className="detail" id="nameInfo">
                  {userInfo?.full_name}
                </td>
              </tr>
              {/* Số điện thoại */}
              <tr>
                <td className="title">Số điện thoại</td>
                <td className="detail" id="phoneInfo">
                  {userInfo?.phone}
                </td>
              </tr>
              {/* Email */}
              <tr>
                <td className="title">Email</td>
                <td className="detail" id="mailInfo">
                  {userInfo?.email}
                </td>
              </tr>
              {/* Địa chỉ */}
              <tr>
                <td className="title">Địa chỉ</td>
                <td className="detail" id="addressInfo">
                  {userInfo?.address}
                </td>
              </tr>
              {/* Địa chỉ nhận hàng */}
              <tr>
                <td className="title">Địa chỉ nhận hàng</td>
                <td className="detail" id="shippingAddressInfo">
                  {userInfo?.shipping_province == "Hồ Chí Minh"
                    ? `
                      ${userInfo?.shipping_address} ${(() => {
                        const quan = district?.find((item) => {
                          return (
                            item.value == Number(userInfo?.shipping_district)
                          );
                        });
                        if (quan) {
                          return quan.label;
                        }
                      })()}
                      ${userInfo?.shipping_province}
                    `
                    : `${userInfo?.shipping_address}`}
                </td>
              </tr>
            </table>
          </div>

          {/* Nút chỉnh sửa */}
          <div className="editButton pt-8">
            <button className="main-border" onClick={showModal}>
              Chỉnh Sửa
            </button>
          </div>
        </div>
      </div>

      {/* Popup Change Personal Info */}
      <Modal
        title="CHỈNH SỬA THÔNG TIN CÁ NHÂN"
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
        centered
      >
        <form onSubmit={formik.handleSubmit}>
          {/* detail */}
          <div className="changePersonalInfo space-y-3">
            {/* Họ tên */}
            <div className="infoItem space-y-2">
              <label htmlFor="full_name">Họ và Tên</label>
              <input
                id="full_name"
                name="full_name"
                // defaultValue={userInfo?.full_name}
                onChange={formik.handleChange}
                value={formik.values.full_name}
              ></input>
            </div>
            {/* Số điện thoại */}
            <div className="infoItem space-y-2">
              <label htmlFor="phone">Số điện thoại</label>
              <input
                id="phone"
                name="phone"
                defaultValue={userInfo?.phone}
                disabled
                // value={formik.values.}
              ></input>
            </div>
            {/* Email */}
            <div className="infoItem space-y-2">
              <label htmlFor="email">Email</label>
              <input
                id="email"
                name="email"
                defaultValue={userInfo?.email}
                onChange={formik.handleChange}
                value={formik.values.email}
              ></input>
            </div>
            {/* Địa chỉ */}
            <div className="infoItem space-y-2">
              <label htmlFor="address">Địa chỉ</label>
              <textarea
                id="address"
                name="address"
                defaultValue={userInfo?.address}
              ></textarea>
            </div>
            {/* Địa chỉ nhận hàng*/}

            <div className="infoItem space-y-1">
              <label>Địa chỉ nhận hàng</label>
              <br />
              {/* select chọn quận, thành phố */}
              <div
                className={
                  formik.values.shipping_province == "Hồ Chí Minh"
                    ? `grid grid-cols-1 sm:grid-cols-2 gap-1 sm:gap-4`
                    : `grid grid-cols-1`
                }
              >
                {/* Chọn Tỉnh/Thành phố */}
                <div className="space-y-1">
                  <p>Tỉnh/Thành phố</p>
                  <Select
                    className="accountSelect"
                    options={[
                      {
                        value: "Hồ Chí Minh",
                        label: "TP Hồ Chí Minh",
                      },
                      {
                        value: "Khác",
                        label: "Tỉnh/TP khác",
                      },
                    ]}
                    style={{
                      width: "100%",
                    }}
                    value={formik.values.shipping_province}
                    onChange={(value) => {
                      // setProvince(value);
                      formik.setFieldValue("shipping_province", value);
                    }}
                  ></Select>
                </div>
                {/* Chọn Quận */}
                {district &&
                  formik.values.shipping_province == "Hồ Chí Minh" && (
                    <div className="space-y-1">
                      <p htmlFor="shipping_district">Quận</p>
                      <Select
                        className="accountSelect"
                        onChange={(value) => {
                          formik.setFieldValue("shipping_district", value);
                        }}
                        defaultValue={() => {
                          if (formik.values.shipping_district) {
                            return Number(formik.values.shipping_district);
                          } else {
                            return "";
                          }
                        }}
                        options={district}
                        style={{
                          width: "100%",
                        }}
                      ></Select>
                    </div>
                  )}
              </div>
              {/* nhập địa chỉ */}
              <div className="space-y-1">
                {formik.values.shipping_province == "Hồ Chí Minh" ? (
                  <p>Số nhà, đường</p>
                ) : (
                  <p>Số nhà,tên đường, quận/huyện, tỉnh thành</p>
                )}

                <textarea
                  id="shipping_address"
                  name="shipping_address"
                  defaultValue={userInfo?.shipping_address}
                  onChange={formik.handleChange}
                ></textarea>
              </div>
            </div>
          </div>
          {/* button action */}
          <div className="actionButton flex space-x-3 pt-4 mt-4 justify-end">
            <button
              className="main-border"
              onClick={handleCancel}
              type="button"
            >
              Hủy
            </button>
            <button className="main-border bg-main-blue" type="submit">
              Lưu
            </button>
          </div>
        </form>
      </Modal>
    </div>
  );
}
