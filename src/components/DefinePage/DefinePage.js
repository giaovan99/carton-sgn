"use client";
import React from "react";
import "../../../css/DefinePage.css";
import { useSelector } from "react-redux";

export default function DefinePage() {
  const { definePage } = useSelector((state) => state.productSlice);
  return (
    <section className="definePage bg-main-gray">
      <div className="container-1 py-4 lg:py-7">
        <h4 className="font-bold">{definePage.title}</h4>
      </div>
    </section>
  );
}
