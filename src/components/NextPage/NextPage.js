import React, { useEffect } from "react";
import { Button, Result } from "antd";
import CountDown from "../CountDown/CountDown";
import { useRouter } from "next/navigation";
export default function NextPage({ title, page, domain, state }) {
  let router = useRouter();
  useEffect(() => {
    if (state) {
      setTimeout(() => {
        router.push(domain);
      }, 5000);
    }
  }, [state]);

  return (
    <div className="nextPage fixed top-0 left-0 w-full h-full bg-white">
      <div className="flex items-center justify-center h-full w-full">
        <Result
          status="success"
          title={title}
          subTitle={
            <>
              Tự động chuyển trang {page} sau{" "}
              <CountDown openCountDown={state} periodTime={5000} />
            </>
          }
          extra={
            <Button
              onClick={() => {
                router.push(domain);
              }}
              className="capitalize"
            >
              {page} ngay
            </Button>
          }
        />
      </div>
    </div>
  );
}
