import { orderService } from "@/services/orderService";
import { getLocalStorage } from "@/utils/localStorage";
import { Modal, Table } from "antd";
import moment from "moment";
import React, { useEffect, useState } from "react";

const RenderListOrder = () => {
  const [page, setPage] = useState(1);
  const [total, setTotal] = useState(0);
  const [dataSource, setDataSource] = useState([]);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [order, setOrder] = useState(null);
  console.log(order);
  useEffect(() => {
    getAPI();
  }, [page]);
  console.log(dataSource);
  const getAPI = async () => {
    let token = getLocalStorage("access_token").token;
    let res = await orderService.history({
      token: token,
      page: page,
    });
    if (res.data.success) {
      setDataSource(res.data.data.data);
      setTotal(res.data.data.total);
    } else {
      alert("Đã có lỗi xảy ra!");
    }
  };
  const columns = [
    {
      title: "Mã đơn hàng",
      dataIndex: "code",
      key: "code",
    },
    {
      title: "Ngày đặt hàng",
      dataIndex: "created_at",
      key: "created_at",
      render: (_, record) => (
        <p>
          {record?.created_at
            ? moment.utc(record?.created_at).format("HH:mm DD/MM/YYYY")
            : ""}
        </p>
      ),
    },
    {
      title: "Trạng thái đơn hàng",
      dataIndex: "status_name",
      key: "status_name",
    },
    {
      title: "Thành tiền (VND)",
      dataIndex: "total",
      key: "total",
      render: (_, record) => (
        <p className="text-right">
          {record?.total ? parseInt(record.total).toLocaleString("en-US") : ""}
        </p>
      ),
    },
    {
      title: "PTTT",
      dataIndex: "payments_type_name",
      key: "payments_type_name",
    },
    {
      title: "Thông tin người nhận",
      dataIndex: "receiver_info",
      key: "receiver_info",
      render: (_, record) => (
        <div>
          <p className="font-semibold">
            {record?.receiver_name ? record?.receiver_name : ""}
          </p>
          <p>Sdt: {record?.receiver_phone ? record?.receiver_phone : ""}</p>
          <p>Email: {record?.receiver_email ? record?.receiver_email : ""}</p>
          <p>
            Địa chỉ: {record?.shipping_address ? record?.shipping_address : ""}{" "}
            {record?.shipping_district_name
              ? record?.shipping_district_name
              : ""}{" "}
            {record?.shipping_province_name
              ? record?.shipping_province_name
              : ""}
          </p>
        </div>
      ),
    },
    {
      title: "Sản phẩm",
      dataIndex: "list_products",
      key: "list_products",
      render: (_, record) => (
        <div className="text-center">
          <button
            onClick={() => {
              showModal(record);
            }}
          >
            <svg
              height="15px"
              viewBox="0 -18 456.212 456"
              width="15px"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path d="m415.007812 1.589844c-1.570312-.96875-3.378906-1.484375-5.222656-1.484375h-297.789062c-25.601563 0-46.429688 20.625-46.429688 45.902343l-1.75 256.441407h-53.816406c-5.523438 0-10 4.476562-10 10v46.199219c.0351562 33.925781 27.527344 61.417968 61.453125 61.457031.6875 0 1.378906-.070313 2.054687-.210938h248.457032c1.480468.105469 2.972656.175781 4.476562.175781 34.453125-.0625 62.351563-28 62.363282-62.453124v-149.90625h67.40625c5.523437 0 10-4.476563 10-10v-148.445313c-.121094-23.871094-17.601563-44.101563-41.203126-47.675781zm-395.007812 357.0625v-36.199219h234.078125v35.167969c-.011719 15.667968 5.882813 30.761718 16.511719 42.273437h-210.503906c-.472657.003907-.941407.039063-1.40625.109375-21.753907-1.480468-38.652344-19.546875-38.679688-41.351562zm338.804688-309.386719v308.351563c-.019532 11.179687-4.460938 21.890624-12.355469 29.800781-7.558594 7.753906-17.828125 12.28125-28.648438 12.625-.539062-.09375-1.085937-.140625-1.636719-.144531h-3.515624c-21.78125-2.144532-38.425782-20.386719-38.570313-42.273438v-45.175781c0-5.519531-4.476563-10-10-10h-180.261719l1.75-256.371094c.144532-14.460937 11.964844-26.082031 26.429688-25.972656h256.742187c-6.441406 8.355469-9.9375 18.609375-9.933593 29.160156zm77.40625 138.445313h-57.40625v-138.445313c.261718-15.667969 13.035156-28.226563 28.703124-28.226563 15.667969 0 28.441407 12.558594 28.703126 28.226563zm0 0" />
              <path d="m128.386719 108.441406h94.710937c5.523438 0 10-4.476562 10-10 0-5.523437-4.476562-10-10-10h-94.710937c-5.523438 0-10 4.476563-10 10 0 5.523438 4.476562 10 10 10zm0 0" />
              <path d="m305.058594 159.941406h-176.671875c-5.523438 0-10 4.476563-10 10 0 5.519532 4.476562 10 10 10h176.671875c5.523437 0 10-4.480468 10-10 0-5.523437-4.476563-10-10-10zm0 0" />
              <path d="m305.058594 231.4375h-176.671875c-5.523438 0-10 4.480469-10 10 0 5.523438 4.476562 10 10 10h176.671875c5.523437 0 10-4.476562 10-10 0-5.519531-4.476563-10-10-10zm0 0" />
            </svg>
          </button>
        </div>
      ),
    },
  ];
  const columnsProducts = [
    {
      title: "Sản phẩm",
      dataIndex: "description",
      key: "description",
    },
    {
      title: "Giá",
      dataIndex: "unit_price",
      key: "unit_price",
      render: (_, record) => (
        <p className="text-right">
          {record?.unit_price
            ? parseInt(record.unit_price).toLocaleString("en-US")
            : ""}
        </p>
      ),
    },
    {
      title: "Số lượng",
      dataIndex: "qty",
      key: "qty",
      render: (_, record) => (
        <p className="text-center">
          {record?.qty ? parseInt(record.qty).toLocaleString("en-US") : ""}
        </p>
      ),
    },
    {
      title: "Thành tiền (VND)",
      dataIndex: "total_price",
      key: "total_price",
      render: (_, record) => (
        <p className="text-right">
          {record?.total_price
            ? parseInt(record.total_price).toLocaleString("en-US")
            : ""}
        </p>
      ),
    },
  ];

  const onChange = (page) => {
    // console.log(page);
    setPage(page);
  };
  const paginationConfig = {
    total: total,
    onChange: () => onChange,
    current: page,
    position: ["bottomCenter"],
  };

  const showModal = (record) => {
    setIsModalOpen(true);
    setOrder(record);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };

  return (
    <div>
      <div className="container-1 tb-padding-3 ">
        <Table
          dataSource={dataSource}
          columns={columns}
          pagination={paginationConfig}
          scroll={{ x: 900 }}
        />
      </div>

      <Modal
        title="Danh sách sản phẩm"
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
        width={800}
      >
        {order?.details.length > 0 && (
          <Table
            dataSource={order.details}
            columns={columnsProducts}
            pagination={{ position: ["bottomCenter"] }}
            scroll={{ x: 550 }}
          />
        )}
        <div>
          <p className="flex">
            <span className="grow text-right">Tạm tính:</span>
            <span className="w-[180px] inline-block text-right">
              {order?.subTotal
                ? parseInt(order?.subTotal).toLocaleString("en-US")
                : ""}
            </span>
          </p>
          <p className="flex">
            <span className="grow text-right ">Phí vận chuyển:</span>
            <span className="w-[180px] inline-block text-right">
              {order?.shipping_fee
                ? parseInt(order?.shipping_fee).toLocaleString("en-US")
                : ""}
            </span>
          </p>
          <p className="flex">
            <span className="grow text-right">Tổng cộng :</span>
            <span className="font-bold text-red-500 w-[180px] inline-block text-right">
              {order?.total
                ? parseInt(order?.total).toLocaleString("en-US")
                : ""}
            </span>
          </p>
        </div>
      </Modal>
    </div>
  );
};

export default RenderListOrder;
