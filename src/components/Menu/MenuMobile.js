"use client";
import Image from "next/image";
import React, { useEffect, useRef, useState } from "react";
import userIcon from "../../../public/menu/user.png";
import bagIcon from "../../../public/menu/bag.png";
import "../../../css/menuMobile.css";
import DropDownMenu from "../DropDownMenu/DropDownMenu";
import { useSelector } from "react-redux";
import Link from "next/link";
import circleIcon from "../../../public/dropDownMenu/circle.png";
import { getLocalStorage, removeLocalStorage } from "@/utils/localStorage";
import { useRouter } from "next/navigation";
import { message } from "antd";
import { HiMenuAlt1, HiX } from "react-icons/hi";
import { IoIosArrowDown } from "react-icons/io";
import AnimateHeight from "react-animate-height";

export default function MenuMobile() {
  let { settingObj } = useSelector((state) => state.settingSlice);
  const [height, setHeight] = useState(0);
  let { totalQuantity } = useSelector((state) => state.cartSlice);
  let route = useRouter();
  const [num, setNum] = useState(0);
  const [menuState, setMenuState] = useState(false);

  const navRef = useRef();
  const showNavbar = () => {
    navRef.current.classList.toggle("responsive_nav");
  };

  useEffect(() => {
    setNum(totalQuantity);
  }, [totalQuantity]);

  let logOut = () => {
    removeLocalStorage("access_token");
    message.success("Đăng xuất thành công");
    setMenuState(false);
    if (typeof window !== "undefined") {
      // Client-side-only code
      window.location.reload();
    }
  };

  let checkExpiry = () => {
    let access_token = getLocalStorage("access_token");

    // nếu access_token chưa có (null)=> Đăng nhập/Đăng ký
    if (!access_token) {
      return (
        <>
          <div
            className="product-item space-x-1.5 cursor-pointer "
            onClick={() => {
              setMenuState(false);
              route.replace("/login");
            }}
          >
            <span className="circleIcon">
              <Image src={circleIcon} alt="..." />
            </span>
            <span>Đăng nhập</span>
          </div>
          <div
            className="product-item space-x-1.5 cursor-pointer "
            onClick={() => {
              setMenuState(false);
              route.replace("/sign-up");
            }}
          >
            <span className="circleIcon">
              <Image src={circleIcon} alt="..." />
            </span>
            <span>Đăng ký</span>
          </div>
        </>
      );
    }

    // nếu access_token đã có => check hạn sử dụng
    else {
      let now = new Date();
      // so sánh thời gian
      if (now.getTime() > access_token.expiry) {
        removeLocalStorage("access_token");
        return (
          <>
            <div
              className="product-item space-x-1.5 cursor-pointer "
              onClick={() => {
                setMenuState(false);
                route.replace("/login");
              }}
            >
              <span className="circleIcon">
                <Image src={circleIcon} alt="..." />
              </span>
              <span>Đăng nhập</span>
            </div>
            <div
              className="product-item space-x-1.5 cursor-pointer "
              onClick={() => {
                setMenuState(false);
                route.replace("/sign-up");
              }}
            >
              <span className="circleIcon">
                <Image src={circleIcon} alt="..." />
              </span>
              <span>Đăng ký</span>
            </div>
          </>
        );
      } else {
        return (
          <>
            <div
              className="product-item space-x-1.5 cursor-pointer"
              onClick={() => {
                setMenuState(false);
                route.replace("/account");
              }}
            >
              <span className="circleIcon">
                <Image src={circleIcon} alt="..." />
              </span>
              <span>Trang cá nhân</span>
            </div>
            <p
              className="product-item space-x-1.5 cursor-pointer"
              onClick={logOut}
            >
              <span className="circleIcon">
                <Image src={circleIcon} alt="..." />
              </span>
              <span>Đăng xuất</span>
            </p>
          </>
        );
      }
    }
  };

  return (
    <section className="bg-main-blue myHeader" id="menuMobile">
      <div className="container-1 flex text-white relative py-4 justify-between">
        <div>
          <button className="nav-btn" onClick={showNavbar}>
            <HiMenuAlt1 />
          </button>
        </div>
        <nav ref={navRef}>
          <div>
            <button className="nav-btn nav-close-btn" onClick={showNavbar}>
              <HiX />
            </button>
          </div>

          {/* Trang chủ */}
          <Link
            href="/"
            className="hover-underline uppercase"
            onClick={showNavbar}
          >
            Trang chủ
          </Link>

          {/* Sản phẩm */}
          <div className="hover-underline relative dropDownList2 cursor-pointer">
            <div
              className="flex justify-between"
              onClick={() => setHeight(height === 0 ? "auto" : 0)}
            >
              <p className="uppercase">Sản phẩm</p>
              <p className={`arrow-down ${height == 0 ? "" : "rotate-180"}`}>
                <IoIosArrowDown />
              </p>
            </div>
            <AnimateHeight
              id="example-panel"
              duration={500}
              height={height} // see props documentation below
            >
              <DropDownMenu showNavbar={showNavbar} />
            </AnimateHeight>
          </div>

          {/* giới thiệu */}
          <Link
            href="/gioi-thieu"
            className="hover-underline uppercase"
            onClick={showNavbar}
          >
            <p>Giới thiệu</p>
          </Link>

          {/* dịch vụ */}
          {/* <Link href="#" className="hover-underline uppercase">
            Dịch vụ
          </Link> */}

          {/* tin tức */}
          <Link
            href="/news-list"
            className="hover-underline uppercase"
            onClick={showNavbar}
          >
            <p>Tin tức</p>
          </Link>

          {/* liên hệ */}
          <Link
            href="/contact"
            className="hover-underline uppercase"
            onClick={showNavbar}
          >
            <p>liên hệ</p>
          </Link>
        </nav>

        <div className="space-x-4 flex items-center">
          {/* trang cá nhân */}
          {settingObj.ecommerce && (
            <div className="userIcon inline-block relative ">
              <p
                className="hover-underline"
                onClick={() => {
                  setMenuState(!menuState);
                }}
              >
                <span className="cartIcon">
                  <Image src={userIcon} alt="..." />
                </span>
              </p>
              {menuState && (
                <div
                  className=" bg-white text-black absolute bottom-[-114px] left-[-172px] z-30"
                  id="dropDownMenu"
                >
                  {checkExpiry()}
                </div>
              )}
            </div>
          )}
          {/* giỏ hàng */}
          <Link href="/cart" className="hover-underline">
            <div className="cartIcon">
              <Image src={bagIcon} alt="..." />
              <p className="itemCart bg-main-red">{num}</p>
            </div>
          </Link>
        </div>
      </div>
    </section>
  );
}
