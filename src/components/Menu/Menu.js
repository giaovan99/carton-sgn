"use client";
import React, { useEffect, useState } from "react";

import useWindowSize from "@/hooks/useWindowSize";
import MenuDesktop from "./MenuDesktop";
import MenuMobile from "./MenuMobile";
export default function Menu() {
  const [isMobile, setIsMobile] = useState(false);
  const [isScroll, setIsScroll] = useState(false);
  const { widthWindow } = useWindowSize();
  useEffect(() => {
    if (widthWindow <= 1023.98) {
      // console.log('window.innerWidth: ', widthWindow);
      setIsMobile(true);
    } else {
      setIsMobile(false);
    }
  });

  useEffect(() => {
    let header;
    window.onscroll = function () {
      myFunction();
    };

    if (isMobile) {
      header = document.getElementById("menuMobile");
    } else {
      header = document.getElementById("menu");
    }
    let sticky = header.offsetTop;
    let leftSide = document.getElementById("left-side-menu");

    function myFunction() {
      if (window.pageYOffset > sticky) {
        header?.classList.add("fixed", "top-0", "left-0", "right-0", "z-50");
        leftSide?.classList.remove("bg-main-blue-2", "py-6");
        setIsScroll(true);
      } else {
        header?.classList.remove("fixed", "top-0", "left-0", "right-0", "z-50");
        leftSide?.classList.add("bg-main-blue-2", "py-6");
        setIsScroll(false);
      }
    }
  });

  return isMobile ? <MenuMobile /> : <MenuDesktop isScroll={isScroll} />;
}
