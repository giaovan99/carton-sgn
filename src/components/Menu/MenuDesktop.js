"use client";
import Image from "next/image";
import React, { useEffect, useState } from "react";
import menuIcon from "../../../public/menu/menuIcon.png";
import logo from "../../../public/menu/logoWhite.png";
import arrowDownIcon from "../../../public/menu/vuesax-linear-arrow-down.png";
import userIcon from "../../../public/menu/user.png";
import bagIcon from "../../../public/menu/bag.png";
import "../../../css/menu.css";
import DropDownMenu from "../DropDownMenu/DropDownMenu";
import { useSelector } from "react-redux";
import Link from "next/link";
import circleIcon from "../../../public/dropDownMenu/circle.png";
import { getLocalStorage, removeLocalStorage } from "@/utils/localStorage";
import { message } from "antd";
export default function MenuDesktop({ isScroll }) {
  let { totalQuantity } = useSelector((state) => state.cartSlice);
  let { settingObj } = useSelector((state) => state.settingSlice);
  const [num, setNum] = useState(0);

  useEffect(() => {
    setNum(totalQuantity);
  }, [totalQuantity]);

  let logOut = () => {
    removeLocalStorage("access_token");
    message.success("Đăng xuất thành công");
    if (typeof window !== "undefined") {
      // Client-side-only code
      window.location.reload();
    }
  };

  let checkExpiry = () => {
    let access_token = getLocalStorage("access_token");

    // nếu access_token chưa có (null)=> Đăng nhập/Đăng ký
    if (!access_token) {
      return (
        <>
          <Link
            className="product-item space-x-1.5 cursor-pointer"
            href="/login"
          >
            <span className="circleIcon">
              <Image src={circleIcon} alt="..." />
            </span>
            <span>Đăng nhập</span>
          </Link>
          <Link
            className="product-item space-x-1.5 cursor-pointer"
            href="/sign-up"
          >
            <span className="circleIcon">
              <Image src={circleIcon} alt="..." />
            </span>
            <span>Đăng ký</span>
          </Link>
        </>
      );
    }

    // nếu access_token đã có => check hạn sử dụng
    else {
      let now = new Date();
      // so sánh thời gian
      if (now.getTime() > access_token.expiry) {
        removeLocalStorage("access_token");
        return (
          <>
            <Link
              className="product-item space-x-1.5 cursor-pointer"
              href="/login"
            >
              <span className="circleIcon">
                <Image src={circleIcon} alt="..." />
              </span>
              <span>Đăng nhập</span>
            </Link>
            <Link
              className="product-item space-x-1.5 cursor-pointer"
              href="/sign-up"
            >
              <span className="circleIcon">
                <Image src={circleIcon} alt="..." />
              </span>
              <span>Đăng ký</span>
            </Link>
          </>
        );
      } else {
        return (
          <>
            <Link
              className="product-item space-x-1.5 cursor-pointer"
              href="/account"
            >
              <span className="circleIcon">
                <Image src={circleIcon} alt="..." />
              </span>
              <span>Trang cá nhân</span>
            </Link>
            <p
              className="product-item space-x-1.5 cursor-pointer"
              onClick={logOut}
            >
              <span className="circleIcon">
                <Image src={circleIcon} alt="..." />
              </span>
              <span>Đăng xuất</span>
            </p>
          </>
        );
      }
    }
  };

  return (
    <section className="bg-main-blue myHeader" id="menu">
      <div className="container-1 flex text-white relative">
        {/* Danh muc san pham */}
        <div
          className="left-side basis-1/5 py-6 dropDownList relative bg-main-blue-2"
          id="left-side-menu"
        >
          {isScroll == false ? (
            <>
              <p className="space-x-3 flex items-center transition duration-500 ease-in-out">
                <span className="icon">
                  <Image src={menuIcon} alt="..." />
                </span>
                <span className="uppercase">DANH MỤC SẢN PHẨM</span>
              </p>
              <div className="dropDownMenu">
                <DropDownMenu />
              </div>
            </>
          ) : (
            <div className="ml-[-24px] flex items-center h-full transition duration-500 ease-in-out">
              <Link href="/" className="w-[120px]">
                <Image src={logo} alt="Logo" />
              </Link>
            </div>
          )}
        </div>

        {/* Khac */}
        <nav className="right-side basis-4/5 py-6 flex justify-end space-x-8 items-center">
          {/* trang chủ */}
          <Link href="/" className="hover-underline uppercase">
            <p>Trang chủ</p>
          </Link>
          {/* dropdown sản phẩm */}
          <div className="hover-underline relative dropDownList">
            <div>
              <p className="uppercase inline-block space-x-2">
                Sản phẩm{" "}
                <span className="arrow-down">
                  <Image src={arrowDownIcon} alt="..." />
                </span>
              </p>
            </div>
            <div className="dropDownMenu">
              <DropDownMenu />
            </div>
          </div>
          {/* giới thiệu */}
          <Link href="/gioi-thieu" className="hover-underline uppercase">
            <p>Giới thiệu</p>
          </Link>
          {/* tin tức */}
          <Link href="/news-list" className="hover-underline uppercase">
            <p>Tin tức</p>
          </Link>
          {/* liên hệ */}
          <Link href="/contact" className="hover-underline uppercase">
            <p>liên hệ</p>
          </Link>
          <div className="space-x-4 ">
            {/* trang cá nhân */}
            {settingObj.ecommerce && (
              <div className="userIcon inline-block relative ">
                <p className=" hover-underline">
                  <span className="cartIcon">
                    <Image src={userIcon} alt="..." />
                  </span>
                </p>
                <div
                  className=" bg-white text-black absolute z-10"
                  id="dropDownMenu"
                >
                  {checkExpiry()}
                </div>
              </div>
            )}
            {/* giỏ hàng */}
            <Link href="/cart" className="hover-underline">
              <span className="cartIcon">
                <Image src={bagIcon} alt="..." />
                <p className="itemCart bg-main-red">{num}</p>
              </span>
            </Link>
          </div>
        </nav>
      </div>
    </section>
  );
}
