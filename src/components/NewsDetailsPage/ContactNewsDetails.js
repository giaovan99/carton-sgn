import React from "react";

export default function ContactNewsDetails() {
  return (
    <div className=" contactNewsDetails mb-12 lg:mb-24">
      <div className="container-1 space-y-7">
        {/* liên hệ */}
        <div className="space-y-4 ">
          <p className="font-bold">Mọi chi tiết xin liên hệ</p>
          <ul className="list-none">
            <li>
              <p>
                Địa chỉ: 185/3 Đường An Phú Đông 10, P. An Phú Đông, Q.12, HCM (
                Võ Thị Liễu rẻ phải )
              </p>
            </li>
            <li>
              <p>
                Điện thoại:{" "}
                <a href="tel:0901338974" className="text-main-blue">
                  090 1338974
                </a>
              </p>
            </li>
            <li>
              <p>
                Zalo:{" "}
                <a href="tel:0901338974" className="text-main-blue">
                  090 1338974
                </a>
              </p>
            </li>
            <li>
              <p>
                Email:{" "}
                <a
                  href="mailto:thungcartonsgn@gmail.com"
                  className="text-main-blue"
                >
                  thungcartonsgn@gmail.com
                </a>
              </p>
            </li>
            <li>
              <p>
                Website:{" "}
                <a href="/" className="text-main-blue">
                  http://thungcartonsgn.vn/
                </a>
              </p>
            </li>
          </ul>
        </div>
        {/* giới thiệt ngắn */}
        <div>
          <p>
            - Chuyên cung cấp và sản xuất bao bì giấy carton, với kinh nghiệm
            hơn 5 năm làm việc trong nghành sản xuất bao bì.
          </p>
          <p>
            - Ngoài hơn 100 size thùng được sản xuất tồn kho sẳn để giao cho quý
            khách trong 24h. Chúng tôi còn nhận sản xuất thiết kế và in ấn theo
            yêu cầu của khách hàng và giao hàng từ 4-5 ngày làm việc.
          </p>
        </div>
      </div>
    </div>
  );
}
