import React from "react";
import "../../../css/newsDetailsItem.css";

export default function NewsDetailsItem({ item }) {
  return (
    <div className="newsDetailsItem">
      <div className="container-1 tb-padding space-y-11">
        {/* title */}
        <h2>{item.name}</h2>
        {/* content */}
        <div
          dangerouslySetInnerHTML={{ __html: item.content }}
          id="newsDetailsItem_content"
        />
      </div>
    </div>
  );
}
