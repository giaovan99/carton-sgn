import React, { useEffect, useState } from "react";
import "../../../../css/ShippingInfo.css";
import "../../../../css/PaymentMethod.css";
import "../../../../css/OrderSummary.css";
import codIcon from "../../../../public/svg-icon/cash-on-delivery.svg";
import paymentIcon from "../../../../public/svg-icon/credit-card.svg";
import qrCode from "../../../../public/qrCode/qr.png";
import Image from "next/image";
import Link from "next/link";
import { useFormik } from "formik";
import { useDispatch, useSelector } from "react-redux";
import defautImg from "../../../../public/defautImg/defautImg.jpg";
import {
  getLocalStorage,
  removeLocalStorage,
  setLocalStorage,
} from "@/utils/localStorage";
import { authService } from "@/services/authService";
import { userService } from "@/services/customerService";
import { useRouter } from "next/navigation";
import { Select, message } from "antd";
import { orderService } from "@/services/orderService";
import { setEmptyCart } from "@/redux/slice/cartSlice";
import NextPage from "@/components/NextPage/NextPage";
import * as yup from "yup";

export default function FormCartPayment() {
  let dispatch = useDispatch();
  const { cart } = useSelector((state) => state.cartSlice);
  const [productArr, setProductArr] = useState([]);
  const [onChangeName, setOnChangeName] = useState(0);
  const [totalCostOfGoods, setTotalCostOfGoods] = useState(0);
  const [shipmentFee, setShipmentFee] = useState(0);
  // const [discount, setDiscount] = useState(0);
  const [totalCost, setTotalCost] = useState(0);
  const [nextPage, setNextPage] = useState(false);
  const [district, setDistrict] = useState();
  const [paymentsType, setPaymentsType] = useState();
  let router = useRouter();

  const formik = useFormik({
    initialValues: {
      full_name: "",
      phone: "",
      email: "",
      shipping_address: "",
      shipping_province: "",
      shipping_district: "",
      payments_type: 1,
      request_invoice: false,
      company_name: "",
      company_taxcode: "",
      company_email: "",
      company_address: "",
      save_invoice_info: false,
    },
    validationSchema: yup.object({
      full_name: yup.string().required("Tên không để trống"),
      shipping_address: yup.string().required("Địa chỉ không để trống"),
      shipping_province: yup.string().required("Tỉnh/TP không để trống"),
      payments_type: yup
        .number()
        .required("Phương thức thanh toán không để trống"),
      shipping_district: yup.string().when("shipping_province", {
        is: (shipping_province) => {
          return shipping_province != "Khác";
        },
        then: (schema) => schema.required("Quận không để trống"),
      }),
      request_invoice: yup.boolean(),
      save_invoice_info: yup.boolean(),
      company_name: yup.string().when("request_invoice", {
        is: true,
        then: (schema) => schema.required("Tên công ty không để trống"),
      }),
      company_taxcode: yup.string().when("request_invoice", {
        is: true,
        then: (schema) => schema.required("MST không để trống"),
      }),
      company_email: yup
        .string()
        .email("Email không hợp lệ")
        .when("request_invoice", {
          is: true,
          then: (schema) => schema.required("Email không để trống"),
        }),
      company_address: yup.string().when("request_invoice", {
        is: true,
        then: (schema) => schema.required("Địa chỉ công ty không để trống"),
      }),
    }),

    onSubmit: async (values) => {
      let check = true;
      // cập nhật thông tin tài khoản lên BE
      check = updateInfo(values);

      //tạo đơn hàng
      if (check) {
        let myCart = getLocalStorage("cart");
        let arr = myCart.map((item) => {
          return {
            sku: item.sku,
            description: item.description,
            product_id: item.id,
            unit_price: item.price,
            qty: item.quantity,
          };
        });
        let object = {
          token: getLocalStorage("access_token").token,
          details: arr,
          receiver_name: values.full_name,
          receiver_phone: values.phone,
          receiver_email: values.email,
          payments_type: values.payments_type,
          shipping_province: values.shipping_province,
          shipping_district: values.shipping_district,
          shipping_address: values.shipping_address,
          request_invoice: values.request_invoice === false ? 0 : 1,
          save_invoice_info: values.save_invoice_info === false ? 0 : 1,
          company_name: values.company_name,
          company_taxcode: values.company_taxcode,
          company_email: values.company_email,
          company_address: values.company_address,
        };
        createOrderApi(object);
      }
    },
  });

  // lấy danh sách quận
  let getDistrict = async () => {
    let res = await orderService.district().catch((err) => {
      console.log(err);
    });
    if (res.data.success == true) {
      let newArr = res.data.data.map((item) => {
        return {
          value: item.id,
          label: `${item.type} ${item.name}`,
        };
      });
      setDistrict(newArr);
    }
  };

  // lấy tài khoản ngân hàng
  let getBankInfo = async () => {
    let res = await orderService.paymentType();
    if (res.data.success) {
      setPaymentsType(res.data.data);
    }
  };

  // resetLogin, lay danh sach san pham, lay danh sach quan, lay tai khoan ngan hang
  useEffect(() => {
    resetLogin();
    getBankInfo();
    setProductArr(cart);
    getDistrict();
  }, []);

  // tinh lai chi phi
  useEffect(() => {
    let sumCOG = 0;
    let sumCost = 0;
    productArr.map((item) => {
      sumCOG += item.quantity * item.price;
    });
    sumCost = sumCOG + Number(shipmentFee);
    setTotalCostOfGoods(sumCOG);
    setTotalCost(sumCost);
  }, [productArr, shipmentFee]);

  // tinh lai phi van chuyen
  useEffect(() => {
    renderShippingFee();
  }, [totalCostOfGoods, formik.values.shipping_district]);

  // render danh sách sản phẩm có trong cart
  let renderCartTable = () => {
    return productArr.map((item, index) => {
      return (
        <tr key={index}>
          <td>
            <div className="relative w-[50px] h-[50px] lg:w-[70px] lg:h-[70px]">
              {item.image == "" ? (
                <Image src={defautImg} alt="..." />
              ) : (
                <img src={item.image} alt="..." />
              )}
              <p className="itemQuantity ">{item.quantity}</p>
            </div>
          </td>
          <td className="px-8">{item.name}</td>
          <td className="text-right">
            {(item.price * item.quantity).toLocaleString("en-US")} VNĐ
          </td>
        </tr>
      );
    });
  };

  // render tổng tiền
  let renderCostList = () => {
    return (
      <table className="costList w-full">
        <tr>
          <td>Tổng tiền hàng</td>
          <td>{totalCostOfGoods.toLocaleString("en-US")} VNĐ</td>
        </tr>
        <tr>
          <td>Phí vận chuyển</td>
          <td>{Number(shipmentFee).toLocaleString("en-US")} VNĐ</td>
        </tr>
        {/* <tr>
          <td>Khuyến mãi</td>
          <td>{discount.toLocaleString("en-US")} VNĐ</td>
        </tr> */}
        <tr>
          <td>Tổng tiền thanh toán</td>
          <td className="font-bold text-red-500">
            {totalCost.toLocaleString("en-US")} VNĐ
          </td>
        </tr>
      </table>
    );
  };

  // render phí vận chuyển
  let renderShippingFee = async () => {
    let res = await orderService.shipment({
      district_id: formik.values.shipping_district,
      price: totalCostOfGoods,
    });
    if (res.data.success) {
      setShipmentFee(res.data.data.fee);
    }
  };

  // lấy thông tin người dùng và render ra HTML
  let getUserInfo = async (token) => {
    let res = await userService.getInfo(token).catch((error) => {
      console.log(error);
    });
    if (res.status == 200) {
      let user = res.data;
      for (let key in user) {
        if (!user[key]) {
          user[key] = "";
        }
      }
      if (user.full_name) {
        setOnChangeName(1);
      }
      formik.setValues({ ...user, payments_type: 1 });
    }
  };

  // reset Token để chuẩn bị tạo đơn hàng và lấy thông tin người dùng
  let resetLogin = async () => {
    let userLogin = getLocalStorage("userLogin");
    let access_token = getLocalStorage("access_token");
    if (!userLogin || !access_token) {
      router.replace("/login");
    } else {
      let res = await authService.userLogin(userLogin).catch((error) => {
        console.log(error);
      });
      if (res.status == 200) {
        let now = new Date();
        const access_token = {
          token: res.data.access_token,
          expiry: now.getTime() + res.data.expires_in * 1000,
        };
        setLocalStorage("access_token", JSON.stringify(access_token));
        getUserInfo(access_token.token);
      }
    }
  };

  // cập nhật thông tin tài khoản
  let updateInfo = async (values) => {
    let res = await userService
      .updateAccountInfo({
        token: getLocalStorage("access_token").token,
        // full_name: values.full_name,
        address: values.address,
        shipping_address: values.shipping_address,
        // email: values.email,
        shipping_province: values.shipping_province,
        shipping_district: values.shipping_district,
      })
      .catch((err) => {
        console.log(err);
        return false;
      });
    if (res.data.code == 200) {
      // message.success("Cập nhật thông tin cá nhân thành công");
      return true;
    }
  };

  // tạo đơn hàng
  let createOrderApi = async (data) => {
    let res = await orderService.createOrder(data).catch((err) => {
      console.log(err);
      message.error("Đặt hàng thất bại, vui lòng tải lại trang và thử lại");
    });
    if (res.data.success) {
      removeLocalStorage("cart");
      dispatch(setEmptyCart());
      setNextPage(true);
    } else {
      message.error("Opps, đã có lỗi xảy ra");
      console.log(res);
    }
  };

  return (
    <div className="cartPaymentInfo flex flex-col-reverse container-1 tb-padding-3 lg:grid lg:grid-cols-2">
      <div className="rightSide lg:pr-[50px] lg:border-r lg:border-[#ECECEC] space-y-10 lg:space-y-14">
        <form
          onSubmit={formik.handleSubmit}
          className="FormCartPayment space-y-14"
        >
          {/* ShippingInfo */}
          <div className="ShippingInfo space-y-6">
            {/* Title */}
            <h4 className="title text-main-blue font-bold">
              Thông tin giao hàng
            </h4>
            {/* confirm thông tin khách hàng */}
            <div className="confirmInfo space-y-3.5">
              {/* Họ và tên */}
              <div className="formInfo space-y-1.5">
                <label htmlFor="name">
                  Họ và Tên<span className="text-red-500">*</span>
                </label>
                <input
                  value={
                    formik.values?.full_name ? formik.values?.full_name : ""
                  }
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  id="name"
                  name="full_name"
                  disabled={onChangeName ? true : false}
                />
                {formik.errors.full_name && (
                  <p
                    className={`nameWarning text-sm ${
                      !formik.touched.full_name
                        ? "text-blue-500"
                        : "text-red-500"
                    }`}
                  >
                    {formik.errors.full_name}
                  </p>
                )}
              </div>
              {/* Số điện thoại và Email */}
              <div className="grid grid-cols-1 lg:grid-cols-2 gap-4">
                {/* Số điện thoại */}
                <div className="formInfo space-y-1.5">
                  <label htmlFor="phone">
                    Số điện thoại<span className="text-red-500">*</span>
                  </label>
                  <input
                    value={formik.values.phone}
                    id="phone"
                    name="phone"
                    disabled
                  />
                </div>
                {/* Email */}
                <div className="formInfo space-y-1.5">
                  <label htmlFor="email">Email</label>
                  <input
                    defaultValue={formik.values.email}
                    onChange={formik.handleChange}
                    id="email"
                    name="email"
                  />
                </div>
              </div>
              {/* Địa chỉ nhận hàng */}
              <div className="shippingAddress">
                <p className="py-4 border-b border-[#ECECEC]">
                  Giao hàng tận nơi
                </p>
                {/* Địa chỉ nhận hàng */}
                <div className="formInfo">
                  <div className="py-4 space-y-4">
                    {/* Quận & TP */}
                    <div
                      className={
                        formik.values.shipping_province == "Hồ Chí Minh"
                          ? `grid grid-cols-1 sm:grid-cols-2 gap-4 `
                          : `grid grid-cols-1 `
                      }
                    >
                      {/* Chọn Tỉnh/Thành phố */}
                      <div className="space-y-1" id="addressArea">
                        <p>
                          Tỉnh/Thành phố <span className="text-red-500">*</span>
                        </p>
                        <Select
                          className="accountSelect"
                          onBlur={() => {
                            formik.setFieldTouched("shipping_province", true);
                          }}
                          options={[
                            {
                              value: "",
                              label: "Chọn Tỉnh/Thành phố",
                            },
                            {
                              value: "Hồ Chí Minh",
                              label: "TP Hồ Chí Minh",
                            },
                            {
                              value: "Khác",
                              label: "Tỉnh/TP khác",
                            },
                          ]}
                          style={{
                            width: "100%",
                          }}
                          value={formik.values.shipping_province}
                          onChange={(value) => {
                            formik.setFieldValue("shipping_province", value);
                            if (value !== "Hồ Chí Minh") {
                              formik.setFieldValue("shipping_district", null);
                            }
                          }}
                        ></Select>
                        {formik.errors.shipping_province && (
                          <p
                            className={`nameWarning text-sm ${
                              !formik.touched.shipping_province
                                ? "text-blue-500"
                                : "text-red-500"
                            }`}
                          >
                            {formik.errors.shipping_province}
                          </p>
                        )}
                      </div>
                      {/* Chọn Quận */}
                      {district &&
                        formik.values.shipping_province == "Hồ Chí Minh" && (
                          <div className="space-y-1">
                            <p htmlFor="shipping_district">
                              Quận <span className="text-red-500">*</span>
                            </p>
                            <Select
                              className="accountSelect"
                              onBlur={() => {
                                formik.setFieldTouched(
                                  "shipping_district",
                                  true
                                );
                              }}
                              onChange={(value) => {
                                formik.setFieldValue(
                                  "shipping_district",
                                  value
                                );
                              }}
                              defaultValue={() => {
                                if (formik.values.shipping_district) {
                                  return Number(
                                    formik.values.shipping_district
                                  );
                                } else {
                                  return "";
                                }
                              }}
                              options={district}
                              style={{
                                width: "100%",
                              }}
                            ></Select>
                            {formik.errors.shipping_district && (
                              <p
                                className={`nameWarning text-sm ${
                                  !formik.touched.shipping_district
                                    ? "text-blue-500"
                                    : "text-red"
                                }`}
                              >
                                {formik.errors.shipping_district}
                              </p>
                            )}
                          </div>
                        )}
                    </div>
                    <div className="space-y-1.5">
                      {formik.values.shipping_province == "Hồ Chí Minh" ? (
                        <label htmlFor="shippingAddress">Số nhà, đường</label>
                      ) : (
                        <label htmlFor="shippingAddress">
                          Số nhà,tên đường, quận/huyện, tỉnh thành
                        </label>
                      )}

                      <span className="text-red-500">*</span>
                      <input
                        id="shippingAddress"
                        name="shipping_address"
                        onBlur={formik.handleBlur}
                        onChange={formik.handleChange}
                        value={formik.values.shipping_address}
                      ></input>
                      {formik.errors.shipping_address && (
                        <p
                          className={`nameWarning text-sm ${
                            !formik.touched.shipping_address
                              ? "text-blue-500"
                              : "text-red-500"
                          }`}
                        >
                          {formik.errors.shipping_address}
                        </p>
                      )}
                    </div>
                  </div>
                </div>
              </div>
              {/* Yêu cầu xuất hoá đơn */}
              <div>
                {/* checkbox */}
                <div className="flex items-center space-x-3">
                  <input
                    id="request_invoice"
                    name="payments_type"
                    onChange={(e) => {
                      let value = e.target.checked;
                      formik.setFieldValue("request_invoice", value);
                      document
                        .querySelector(".requestInfo")
                        .classList.toggle("hidden");
                    }}
                    onBlur={formik.handleBlur}
                    type="checkbox"
                    value={formik.values.request_invoice}
                  ></input>
                  <label htmlFor="request_invoice" className="space-x-2">
                    <span>Yêu cầu xuất hoá đơn</span>
                  </label>
                </div>
                {/* thông tin */}
                <div className="requestInfo hidden mt-3">
                  <p className="font-bold">Thông tin xuất hoá đơn</p>
                  <div className="grid grid-cols-1 lg:grid-cols-2 gap-2">
                    {/*Tên công ty*/}
                    <div className="formInfo col-span-full">
                      <label htmlFor="company_name">
                        Tên công ty<span className="text-red-500">*</span>
                      </label>
                      <input
                        value={formik.values.company_name}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        id="company_name"
                        name="company_name"
                      ></input>
                      {formik.errors.company_name && (
                        <p
                          className={`nameWarning text-sm ${
                            !formik.touched.company_name
                              ? "text-blue-500"
                              : "text-red-500"
                          }`}
                        >
                          {formik.errors.company_name}
                        </p>
                      )}
                    </div>
                    {/*Mã số thuế*/}
                    <div className="formInfo">
                      <label htmlFor="company_taxcode">
                        Mã số thuế<span className="text-red-500">*</span>
                      </label>
                      <input
                        value={formik.values.company_taxcode}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        id="company_taxcode"
                        name="company_taxcode"
                      ></input>
                      {formik.errors.company_taxcode && (
                        <p
                          className={`nameWarning text-sm ${
                            !formik.touched.company_taxcode
                              ? "text-blue-500"
                              : "text-red-500"
                          }`}
                        >
                          {formik.errors.company_taxcode}
                        </p>
                      )}
                    </div>
                    {/*Email nhận hoá đơn*/}
                    <div className="formInfo">
                      <label htmlFor="company_email">
                        Email nhận hoá đơn
                        <span className="text-red-500">*</span>
                      </label>
                      <input
                        value={formik.values.company_email}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        id="company_email"
                        name="company_email"
                      ></input>
                      {formik.errors.company_email && (
                        <p
                          className={`nameWarning text-sm ${
                            !formik.touched.company_email
                              ? "text-blue-500"
                              : "text-red-500"
                          }`}
                        >
                          {formik.errors.company_email}
                        </p>
                      )}
                    </div>
                    {/*Địa chỉ công ty*/}
                    <div className="formInfo col-span-full">
                      <label htmlFor="company_address">
                        Địa chỉ công ty<span className="text-red-500">*</span>
                      </label>
                      <input
                        value={formik.values.company_address}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        id="company_address"
                        name="company_address"
                      ></input>
                      {formik.errors.company_address && (
                        <p
                          className={`nameWarning text-sm ${
                            !formik.touched.company_address
                              ? "text-blue-500"
                              : "text-red-500"
                          }`}
                        >
                          {formik.errors.company_address}
                        </p>
                      )}
                    </div>
                    <div className="mt-2 flex items-center gap-2 col-span-full justify-end">
                      {" "}
                      <input
                        id="save_invoice_info"
                        name="save_invoice_info"
                        onChange={(e) => {
                          let value = e.target.checked;
                          formik.setFieldValue("save_invoice_info", value);
                        }}
                        onBlur={formik.handleBlur}
                        type="checkbox"
                        value={formik.values.save_invoice_info}
                      ></input>
                      <label
                        htmlFor="save_invoice_info"
                        className="space-x-2 italic text-gray-500"
                      >
                        <span>Lưu lại thông tin xuất hoá đơn</span>
                      </label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* PaymentMethod */}
          <div className="PaymentMethod space-y-6">
            {/* Title */}
            <h4 className="title text-main-blue font-bold">
              Phương thức thanh toán
            </h4>
            {/* Phương thức thanh toán */}
            {paymentsType && (
              <div className="paymentMethodInfo bg-main-gray">
                {/* Thanh toán khi nhận hàng */}
                <div className="px-5 py-4 border-b border-[#ECECEC]">
                  <div className="formInfo space-x-3">
                    <input
                      id="atHome"
                      name="payments_type"
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      type="radio"
                      value={1}
                      checked
                    ></input>
                    <label htmlFor="atHome" className="space-x-2">
                      <Image src={codIcon} alt="..." />
                      <span>Thanh toán khi nhận hàng</span>
                    </label>
                  </div>
                </div>
                {/* Thanh toán chuyển khoản &  Thông tin ngân hàng*/}
                <div className="px-5 py-4 space-y-5">
                  {/* Thanh toán chuyển khoản */}
                  <div className="formInfo space-x-3">
                    <input
                      id="banking"
                      name="payments_type"
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      type="radio"
                      value={2}
                    ></input>
                    <label htmlFor="banking" className="space-x-2">
                      <Image src={paymentIcon} alt="..." />
                      <span>Chuyển khoản thanh toán</span>
                    </label>
                  </div>
                  {/* Thông tin ngân hàng */}
                  <div className="formInfo space-x-3">
                    <input className="invisible"></input>
                    {/* <div
                      dangerouslySetInnerHTML={{
                        __html: paymentsType[1].content,
                      }}
                    /> */}
                    <div>
                      <p className="font-bold">
                        CÔNG TY TNHH ĐẦU TƯ TM SÀI GÒN NGUYỄN
                      </p>
                      <p>Ngân hàng: Vietinbank - CN11,TP. Hồ Chí Minh</p>
                      <p>STK: 119002858828</p>
                      <p className="text-center mt-1">
                        <Image src={qrCode} className="w-[250px]" />
                      </p>
                    </div>
                  </div>
                </div>
                {/* Thanh toán khi nhận hàng */}
                <div className="px-5 ">
                  {formik.errors.payments_type && (
                    <p
                      className={`nameWarning text-sm ${
                        !formik.touched.payments_type
                          ? "text-blue-500"
                          : "text-red-500"
                      }`}
                    >
                      {formik.errors.payments_type}
                    </p>
                  )}
                </div>
              </div>
            )}
          </div>
          {/* AcctionButton */}
          <div className="actionButton2 flex flex-col space-y-4 lg:space-y-0 lg:flex-row items-end lg:justify-between lg:items-center ">
            <Link className="underline" href="/cart">
              Giỏ hàng
            </Link>
            <button
              type="submit"
              className="main-border bg-main-red text-white py-3.5 w-[320px] lg:w-[370px] text-center font-bold"
            >
              Hoàn tất đặt hàng
            </button>
          </div>
        </form>
      </div>
      <div className="leftSide  pb-10 lg:mb-0 lg:pl-[50px]">
        <div className="OrderSummary space-y-10 ">
          {/* table sản phẩm trong giỏ hàng */}
          <div className="overflow-x-scroll lg:block">
            {productArr != [] && (
              <table className="cartTable w-full">
                <tbody>{renderCartTable()}</tbody>
              </table>
            )}
          </div>
          {/* costList*/}
          {renderCostList()}
        </div>
      </div>

      {/* overlay */}
      {nextPage && (
        <NextPage
          title="Đặt hàng thành công"
          page="trang chủ"
          domain="/"
          state={nextPage}
        />
      )}
    </div>
  );
}
