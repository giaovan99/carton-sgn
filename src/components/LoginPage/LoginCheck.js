"use client";
import React, { useEffect } from "react";
import FormLoginPage from "./FormLoginPage/FormLoginPage";

export default function LoginCheck() {
  return (
    <div>
      {/* Đăng nhập */}
      <FormLoginPage />
    </div>
  );
}
