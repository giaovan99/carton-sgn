"use client";
import React, { useEffect } from "react";
import "../../../../css/FormLoginPage.css";
import Image from "next/image";
import userIcon from "../../../../public/svg-icon/user.svg";
import lockIcon from "../../../../public/svg-icon/lock.svg";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { useDispatch, useSelector } from "react-redux";
import { setDefinePageTitle } from "@/redux/slice/productSlice";

import {
  getLocalStorage,
  getLocalStorage2,
  removeLocalStorage,
  setLocalStorage,
} from "@/utils/localStorage";
import { authService } from "@/services/authService";
import { message } from "antd";

export default function FormLoginPage() {
  const dispatch = useDispatch();
  const router = useRouter();
  const { definePage } = useSelector((state) => state.productSlice);

  useEffect(() => {
    dispatch(setDefinePageTitle({ title: "Đăng nhập", id: -1 }));
    let savePass = getLocalStorage("userLogin");

    // Kiểm tra xem lần trước khách có lưu mật khẩu không
    if (savePass?.savePassword) {
      document.getElementById("phone").value = savePass.phone;
      document.getElementById("password").value = savePass.password;
      document.getElementById("savePassword").checked = true;
    }
  }, []);

  // check xem lần này khách có chọn lưu mật khẩu ko
  let checkSavePassword = (userData) => {
    let check = document.getElementById("savePassword").checked;
    if (check) {
      setLocalStorage("userLogin", JSON.stringify(userData));
    } else {
      setLocalStorage("userLogin", JSON.stringify(userData));
    }
  };

  let checkLogin = async (event) => {
    event.preventDefault();
    // check api
    const data = new FormData(event.target);
    const inputObject = Object.fromEntries(data);
    let checkPhone = regexPhoneNumber(inputObject.phone);
    if (checkPhone) {
      const res = await authService.userLogin(inputObject);

      if (res.data.code !== 400) {
        document.getElementById("password_warning").classList.add("hidden");
        document.getElementById("password_warning").innerHTML = "";
        let now = new Date();
        // thành công -> đẩy lên local storage + alert "đăng nhập thành công" + chuyển qua trang chủ
        const access_token = {
          token: res.data.access_token,
          expiry: now.getTime() + res.data.expires_in * 1000,
        };
        setLocalStorage("access_token", JSON.stringify(access_token));
        checkSavePassword(inputObject);
        message.success("Đăng nhập thành công");

        // Kiểm tra xem có đang đặt hàng hay không. Nếu có -> trang cart. Nếu không -> trang chủ
        let createOrder = getLocalStorage2("createOrder");

        if (createOrder) {
          removeLocalStorage("createOrder");
          router.replace("/cart");
          router.refresh();
        } else {
          router.refresh();
          router.replace("/");
        }
      } else {
        // thất bại -> alert "đăng nhập thất bại." + hiển thị lỗi
        document.getElementById("password_warning").classList.remove("hidden");
        document.getElementById("password_warning").innerHTML =
          "Đăng nhập thất bại. Kiểm tra lại tài khoản, mật khẩu";
      }
    }
  };

  let regexPhoneNumber = (phone) => {
    event.preventDefault();
    if (phone) {
      const regex = /(84|0[3|5|7|8|9])+([0-9]{8})\b/g;
      var isValid = regex.test(phone);
      if (!isValid) {
        document.getElementById("phone_warning").classList.remove("hidden");
        document.getElementById("phone_warning").innerHTML =
          "Số điện thoại không hợp lệ.";
        document.getElementById("phone").focus();
        return false;
      } else {
        document.getElementById("phone_warning").classList.add("hidden");
        document.getElementById("phone_warning").innerHTML = "";
        return true;
      }
    } else {
      document.getElementById("phone_warning").classList.add("hidden");
      document.getElementById("phone_warning").innerHTML = "";
      return false;
    }
  };

  // regexPassword;
  let regexPassword = (pass) => {
    if (pass) {
      // const regex =
      //   /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/;
      // var isValid = regex.test(pass);
      // if (isValid == false || pass.length < 8) {
      //   document.getElementById("password_warning").classList.remove("hidden");
      // // document.getElementById("password_warning").classList.add("flex");
      // document.getElementById("password_warning").innerHTML =
      //   "Mật khẩu yêu cầu ít nhất 8 ký tự (ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)";
      // return false;
      // } else {
      // document.getElementById("password_warning").classList.add("hidden");
      // document.getElementById("password_warning").innerHTML = "";
      // return true;
      // }
    } else {
      document.getElementById("password_warning").classList.add("hidden");
      document.getElementById("password_warning").innerHTML = "";
      return false;
    }
  };

  let resetWarning = () => {
    document.getElementById("phone_warning").classList.add("hidden");
    document.getElementById("phone_warning").innerHTML = "";
    document.getElementById("password_warning").classList.add("hidden");
    document.getElementById("password_warning").innerHTML = "";
  };

  return (
    <div className="formLoginPage">
      <div className="container-1 tb-padding-2">
        <div className="formLG flex flex-col justify-between">
          {/* title */}
          <div className="title text-center font-bold">
            <h4>Đăng nhập</h4>
          </div>

          {/* form */}
          <form className="userInfo space-y-4" onSubmit={checkLogin}>
            <div>
              {/* input Tài khoản */}
              <div className="inputUserInfo flex space-x-2.5">
                <label htmlFor="phone">
                  <Image
                    src={userIcon}
                    alt="..."
                    className="iconInputUserInfo"
                  />
                </label>
                <input
                  type="text"
                  placeholder="Số điện thoại"
                  name="phone"
                  id="phone"
                  className="grow"
                  required
                  onInvalid={(e) => {
                    if (e.target.value == "") {
                      e.target.setCustomValidity(
                        "Vui lòng điền số điện thoại!"
                      );
                    } else {
                      e.target.setCustomValidity("");
                    }
                  }}
                  onChange={(event) => {
                    if (event.target.value !== "") {
                      event.target.setCustomValidity("");
                      regexPhoneNumber(event.target.value);
                    } else {
                      document
                        .getElementById("phone_warning")
                        .classList.add("hidden");
                      document.getElementById("phone_warning").innerHTML = "";
                    }
                  }}
                />
              </div>
              <p
                className="mt-1 text-red-500 text-sm hidden"
                id="phone_warning"
              ></p>
            </div>
            <div>
              {/* input Mật khẩu */}
              <div className="inputUserInfo flex space-x-2.5">
                <label htmlFor="password">
                  <Image
                    src={lockIcon}
                    alt="..."
                    className="iconInputUserInfo"
                  />
                </label>
                <input
                  type="password"
                  placeholder="Mật khẩu"
                  name="password"
                  id="password"
                  className="grow"
                  required
                  onInvalid={(e) => {
                    if (e.target.value == "") {
                      e.target.setCustomValidity("Vui lòng điền mật khẩu!");
                    } else {
                      e.target.setCustomValidity("");
                    }
                  }}
                  onChange={(event) => {
                    if (event.target.value !== "") {
                      event.target.setCustomValidity("");
                      regexPassword(event.target.value);
                    } else {
                      document
                        .getElementById("password_warning")
                        .classList.add("hidden");
                      document.getElementById("password_warning").innerHTML =
                        "";
                    }
                  }}
                />
              </div>
              <p
                className="mt-1 text-red-500 text-sm hidden"
                id="password_warning"
              ></p>
            </div>
            {/* tính năng khác */}
            <div className="otherFunctionUserInfo flex justify-between">
              {/* lưu mật khẩu */}
              <div className="savePassword flex  items-center space-x-2">
                <input type="checkbox" id="savePassword" name="savePassword" />
                <label htmlFor="savePassword">Lưu mật khẩu</label>
              </div>
              {/* quên mật khẩu */}
              <div className="forgetPassword flex  items-center space-x-2">
                <Link
                  className="underline hover:text-[#003F93] duration-300"
                  href="/forget-password"
                >
                  Quên mật khẩu?
                </Link>
              </div>
            </div>

            {/* button đăng nhập */}
            <div className="loginButton">
              <button
                className="main-border bg-main-blue w-full py-3.5 text-center font-bold text-white"
                type="submit"
              >
                Đăng Nhập
              </button>
            </div>
          </form>

          {/* chuyển trang đăng ký */}
          <div className="signupForm flex justify-center">
            <p>Bạn chưa có tài khoản?&nbsp;</p>
            <Link
              href="/sign-up"
              className="text-main-blue underline duration-500  hover:font-bold"
            >
              {" "}
              Đăng ký ngay
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}
