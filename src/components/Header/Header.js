"use client";
import React, { useEffect, useRef, useState } from "react";
import Image from "next/image";
import "../../../css/Header.css";
import telephone from "../../../public/header/call.png";
import search from "../../../public/header/search-normal.png";
import Link from "next/link";
import { useDispatch, useSelector } from "react-redux";
import { usePathname, useRouter } from "next/navigation";
import { Select, Space } from "antd";
import { BsArrowLeft } from "react-icons/bs";
import { productService } from "@/services/productService";
import { setArrProductType } from "@/redux/slice/productSlice";
import { getSettingInfo } from "@/utils/setting";
import { setSettingInfo } from "@/redux/slice/settingSlice";
import { setActionSpinner } from "@/redux/slice/spinnerSlice";
import LoadingSpinner from "../fixedItems/LoadingSpinner";

export default function Header() {
  const dispatch = useDispatch();
  const router = useRouter();
  const pathName = usePathname();
  const [message, setMessage] = useState("");
  let [productType, setProductType] = useState("");
  let [cloneArr, setCloneArr] = useState([]);
  let { arrProductType } = useSelector((state) => state.productSlice);
  let { settingObj } = useSelector((state) => state.settingSlice);

  const filterRef = useRef();
  const showFilter = () => {
    filterRef.current.classList.toggle("responsive_transform");
  };

  // functions
  let renderListProduct = (event) => {
    setMessage(event.target.value);
  };

  let handleChange = (value) => {
    arrProductType.map((item) => {
      if (item.id == value) {
        setProductType(item.id);
      } else if (value == 0) {
        setProductType(0);
      }
    });
  };

  let searchListProduct = () => {
    filterRef.current.classList.toggle("responsive_transform");
    let productName = document.getElementById("searchNameField").value;
    if (productName == "") {
      router.push(`/product-list?categoryId=${productType}`);
    } else {
      router.push(
        `/product-list?categoryId=${productType}&keywords=${productName}`
      );
    }
  };

  let getArrProductType = async () => {
    const res = await productService.productCategory();
    if (res.data.code == 200) {
      dispatch(setArrProductType(res.data.data));
    }
  };

  useEffect(() => {
    getArrProductType();
  }, []);

  useEffect(() => {
    let options = arrProductType.map((item) => {
      return {
        value: item.id,
        label: item.name,
      };
    });
    options.unshift({
      value: 0,
      label: "Tất cả",
    });
    setCloneArr(options);
  }, [arrProductType]);

  const getSetting = async () => {
    dispatch(setActionSpinner(true));
    let res = await getSettingInfo();
    if (res.status) {
      dispatch(setSettingInfo(res.data));
      dispatch(setActionSpinner(false));
    }
  };

  useEffect(() => {
    if (!pathName.includes("/product-list")) {
      setMessage("");
      setProductType(0);
    }
    getSetting();
  }, [pathName]);

  return (
    <>
      <LoadingSpinner />
      <header className="container-1 py-4 lg:py-5 flex justify-between">
        {/* logo */}
        <Link href="/" className="logo basis-1/5">
          {settingObj?.logo && <img src={settingObj?.logo} alt="..." />}
        </Link>

        {/* filter & phone */}
        <form
          className="filter-phone flex basis-9/12"
          onSubmit={(event) => {
            event.preventDefault();
            searchListProduct();
          }}
          id="searchHeader"
        >
          <div className="flex w-full filterGroup" ref={filterRef}>
            {/* back */}
            <button
              className="search-icon flex-none lg:hidden mr-4 cursor-pointer"
              onClick={showFilter}
              type="button"
            >
              <BsArrowLeft />
            </button>
            {/* select type */}
            <div className="dropDownSelectMenu flex-none md:rounded-l-[26px]">
              <Space wrap>
                <Select
                  defaultValue="Tất cả"
                  style={{
                    width: 120,
                  }}
                  onChange={handleChange}
                  options={cloneArr}
                  value={productType}
                />
              </Space>
            </div>
            {/* filter */}
            <div className="filter grow md:rounded-r-[26px]">
              <input
                placeholder="Từ khóa"
                className="grow"
                id="searchNameField"
                onChange={renderListProduct}
                value={message}
              />
              <button className="search-icon flex-none " type="submit">
                <Image src={search} alt="..." />
              </button>
            </div>
          </div>
          {/* icon kính lúp */}
          <button
            type="button"
            className="search-icon flex-none lg:hidden"
            onClick={showFilter}
          >
            <Image src={search} alt="..." />
          </button>
          {/* phone call */}
          <div className="flex-none pl-4">
            <a
              className="phone main-border py-3 px-8 bg-main-red flex text-white "
              href={`tel:${settingObj.hotline}`}
            >
              <div className="icon">
                <Image src={telephone} alt="..." />
              </div>
              <h5 className="font-bold pl-2">{settingObj.hotline}</h5>
            </a>
          </div>
        </form>
      </header>
    </>
  );
}
