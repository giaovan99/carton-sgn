import React, { useEffect, useState } from "react";
import NewsItem from "../HomePage/NewsHomePage/NewsItem/NewsItem";
import { Pagination } from "antd";
import { newsService } from "@/services/news";

export default function ListNews() {
  const [newsArray, setNewsArray] = useState();
  const [totalItem, setTotalItem] = useState();

  //   gọi API lần đầu lấy dữ liệu load trang
  useEffect(() => {
    getListNews(1);
  }, []);

  // lấy danh sách news theo trang
  let getListNews = async (page) => {
    let res = await newsService.list({
      limit: 15,
      page: page,
      keywords: "",
      product_category_id: "",
    });
    if (res.data.success == true) {
      setNewsArray(res.data.data);
      setTotalItem(res.data.total);
    }
  };

  //   render danh sách news
  let renderListNews = () => {
    return newsArray.map((item, index) => {
      return <NewsItem item={item} key={index} />;
    });
  };

  //  thay đổi page number
  const onChange = (pageNumber) => {
    getListNews(pageNumber);
    window.scrollTo(0, 0);
  };

  return (
    <div className="listNews" id="listNews">
      <div className="container-1 tb-padding space-y-10">
        {/* danh sách */}
        <div className="renderListNews grid grid-cols-1 lg:grid-cols-3 gap-8">
          {newsArray && renderListNews()}
        </div>
        {/* pagination */}
        <div className="flex justify-center items-center">
          <Pagination
            defaultCurrent={1}
            total={totalItem}
            showSizeChanger={false}
            defaultPageSize={15}
            onChange={onChange}
            hideOnSinglePage={true}
          />
        </div>
      </div>
    </div>
  );
}
