"use client";
import React from "react";
import "../../../css/footer.css";
import Link from "next/link";
import { useSelector } from "react-redux";
import { SiZalo } from "react-icons/si";
import { RiFacebookFill } from "react-icons/ri";
import { MdOutlineMailOutline } from "react-icons/md";
import { CiHeadphones } from "react-icons/ci";

export default function Footer() {
  let { settingObj } = useSelector((state) => state.settingSlice);

  return (
    <footer className="bg-main-blue">
      <div className="container-1 text-white grid grid-cols-1 md:grid-cols-2 xl:grid-cols-4 gap-5">
        {/* Về công ty */}
        <div>
          <p className="p2 uppercase font-bold">VỀ CÔNG TY</p>
          <nav className="mt-2">
            <ul>
              <li>
                <Link href="/gioi-thieu">Giới thiệu</Link>
              </li>
              <li>
                <Link href="/he-thong-cua-hang">Hệ thống cửa hàng</Link>
              </li>
            </ul>
          </nav>
        </div>
        {/* Hỗ trợ KH */}
        <div>
          <p className="p2 uppercase font-bold">HỖ TRỢ KHÁCH HÀNG</p>
          <nav className="mt-2">
            <ul>
              <li>
                <Link href="/dieu-khoan-su-dung">Điều khoản sử dụng</Link>
              </li>
              <li>
                <Link href="/cau-hoi-thuong-gap">Câu hỏi thường gặp</Link>
              </li>
              <li>
                <Link href="/trung-tam-ho-tro-cskh">Trung tâm hỗ trợ CSKH</Link>
              </li>
              <li>
                <Link href="/phuong-thuc-thanh-toan">
                  Phương thức thanh toán
                </Link>
              </li>
            </ul>
          </nav>
        </div>
        {/* Chính sách */}
        <div>
          <p className="p2 uppercase font-bold">Chính sách</p>
          <nav className="mt-2">
            <ul>
              <li>
                <Link href="/chinh-sach-bao-mat">Chính sách bảo mật</Link>
              </li>
              <li>
                <Link href="/chinh-sach-doi-tra">Chính sách đổi trả</Link>
              </li>
              <li>
                <Link href="/chinh-sach-van-chuyen-giao-nhan">
                  Chính sách vận chuyển và giao nhận
                </Link>
              </li>
              <li>
                <Link href="/chinh-sach-giai-quyet-khieu-nai">
                  Chính sách giải quyết khiếu nại
                </Link>
              </li>
            </ul>
          </nav>
        </div>
        {/* Kết nối với công ty */}
        <div>
          <p className="p2 uppercase font-bold">Kết nối với công ty</p>
          <nav className="mt-2">
            <ul className="flex space-x-2">
              {/* zalo OA */}
              <li>
                <a
                  href={`${settingObj.social_zalo}`}
                  className="social-item"
                  target="_blank"
                >
                  <SiZalo />
                </a>
              </li>
              {/* FB */}
              <li>
                <a
                  href={`${settingObj.social_facebook}`}
                  className="social-item"
                  target="_blank"
                >
                  <RiFacebookFill />
                </a>
              </li>
              {/* Mail */}
              <li>
                <a href={`mailto:${settingObj.email}`} className="social-item">
                  <MdOutlineMailOutline />
                </a>
              </li>
              {/* Tổng đài CSKH */}
              <li>
                <a href={`tel:${settingObj.hotline}`} className="social-item">
                  <CiHeadphones />
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
      <div className="container-1 pt-[30px] grid grid-cols-1 md:grid-cols-2 xl:flex xl:flex-row space-y-10 xl:space-y-0 xl:space-x-[1%] text-white justify-between">
        {/* general info */}
        <div className="md:col-span-2 footer-item general-info xl:w-[35%] space-y-2 ">
          <div className="title ">
            <p className="p2 uppercase font-bold">
              CÔNG TY TNHH ĐẦU TƯ THƯƠNG MẠI SÀI GÒN NGUYỄN
            </p>
          </div>
          <div className="disc space-y-2 xl:space-y-4">
            <p className="p2">
              Chuyên cung cấp và sản xuất bao bì giấy carton, với kinh nghiệm
              hơn 5 năm làm việc trong nghành sản xuất bao bì.
            </p>
            <p className="p2">
              Ngoài hơn 100 size thùng được sản xuất tồn kho sẳn để giao cho quý
              khách trong 24h. Chúng tôi còn nhận sản xuất thiết kế và in ấn
              theo yêu cầu của khách hàng và giao hàng từ 4-5 ngày làm việc.
            </p>
          </div>
        </div>
        {/* contact info */}
        <div className="footer-item contact-info space-y-2 xl:w-[33%] ">
          <div className="title">
            <p className="p2 uppercase font-bold">Thông tin liên hệ</p>
          </div>
          <div className="disc space-y-2 xl:space-y-4">
            <div
              dangerouslySetInnerHTML={{ __html: settingObj.address }}
              className="space-y-2 xl:space-y-4"
            ></div>
          </div>
        </div>
        {/* facebook plugin */}
        <div className="footer-item fb-plugin  flex items-start md:items-center justify-center h-[200px] xl:w-[30%]">
          {/* <iframe
            src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fthungcartonsgn&tabs=timeline%2Cmessages&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=false&appId"
            style={{
              border: "none",
              overflow: "hidden",
              width: "max-content",
              height: "100%",
            }}
            scrolling="no"
            frameBorder={0}
            allowFullScreen={true}
            crossOrigin={true}
            allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"
          /> */}
        </div>
      </div>
    </footer>
  );
}
