import { https } from "@/configs/baseUrl";

export const userService = {
  userLogin: async (data) => await https.post(`/customer`, data),
  checkOTP: async (data) => await https.post(`/check-otp`, data),
  create: async (data) => await https.post(`/customer`, data),
  getInfo: async (data) => await https.get(`/customer/profile?token=${data}`),
  updateAccountInfo: async (data) => await https.post(`/update-customer`, data),
  forgetPassword: async (data) => await https.post(`/forgot-password`, data),
  changePassword: async (data) => await https.post(`/change-password`, data),
  getOTP: async (data) => await https.post(`/get-otp`, data),
};
