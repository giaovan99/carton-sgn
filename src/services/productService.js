import { https } from "@/configs/baseUrl";

export const productService = {
  productCategory: async () => await https.get(`/product_category`),
  top: async (data) => await https.get(`/product/featured?type=${data}`),
  list: (data) => {
    return https.get(
      `/product?limit=${data.limit}&page=${data.page}&keywords=${
        data?.keywords ? data.keywords : ""
      }&product_category_id=${data.product_category_id}&sort_order=${
        data?.sort_order ? data.sort_order : ""
      }&sort_field=${data?.sort_field ? data?.sort_field : ""}`
    );
  },
  productDetail: async (slug) => await https.get(`/product/${slug}`),
  getSlugs: async () => await https.get(`/all-slug`),
};
