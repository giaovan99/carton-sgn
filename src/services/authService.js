import { https } from "@/configs/baseUrl";

export const authService = {
  userLogin: async (data) => await https.post(`/auth/login`, data),
};
