import { https } from "@/configs/baseUrl";

export const orderService = {
  createOrder: async (data) => await https.post(`/orders`, data),
  district: async () => await https.get(`/geo_district`),
  shipment: async (data) => await https.post(`/shipment`, data),
  paymentType: async () => await https.get(`/payment_type`),
  history: async (data) =>
    await https.get(`/orders/history?token=${data?.token}`, data),
};
