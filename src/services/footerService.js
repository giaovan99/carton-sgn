import { https } from "@/configs/baseUrl";

export const footerServices = {
  getDetail: async (slug) => {
    try {
      try {
        return await https.get(`/pages/${slug}`);
      } catch (err) {
        console.log(err);
      }
    } catch (err) {
      console.log(err);
    }
  },
  getAllPages: async () => {
    try {
      return await https.get(`/pages`);
    } catch (err) {
      console.log(err);
    }
  },
};
