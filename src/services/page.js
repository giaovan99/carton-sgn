import { https } from "@/configs/baseUrl";

export const pageService = {
  sendContact: async (data) => await https.post(`/contact`, data),
};
