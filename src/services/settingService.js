import { https } from "@/configs/baseUrl";

export const settingService = {
  settingInfo: async () => {
    try {
      return await https.get("/setting");
    } catch (err) {
      console.log(err);
    }
  },
};
