import { https } from "@/configs/baseUrl";

export const newsService = {
  list: async (data) =>
    await https.get(
      `/news?limit=${data.limit}&page=${data.page}&keywords=${data.keywords}&product_category_id=${data.product_category_id}`
    ),
  detail: async (data) => await https.get(`/news/${data}`),
};
