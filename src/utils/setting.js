import { settingService } from "@/services/settingService";

export const getSettingInfo = async () => {
  try {
    let res = await settingService.settingInfo();
    if (res.data.success) {
      return {
        status: true,
        data: res.data.data,
      };
    } else {
      console.log(res);
      return {
        status: false,
      };
    }
  } catch (err) {
    console.log(err);
    return {
      status: false,
    };
  }
};
