export const setLocalStorage = (name, value) => {
  if (typeof window !== "undefined") {
    // Client-side-only code
    return localStorage.setItem(name, value);
  }
};

export const getLocalStorage = (name) => {
  if (typeof window !== "undefined") {
    // Client-side-only code
    return localStorage.getItem(name)
      ? JSON.parse(localStorage.getItem(name) || "")
      : null;
  }
};

export const getLocalStorage2 = (name) => {
  if (typeof window !== "undefined") {
    // Client-side-only code
    return localStorage.getItem(name);
  }
};

export const removeLocalStorage = (name) => {
  if (typeof window !== "undefined") {
    // Client-side-only code
    return localStorage.removeItem(name);
  }
};
