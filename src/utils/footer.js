import { footerServices } from "@/services/footerService";

export const getDetail = async (slug) => {
  try {
    let res = await footerServices.getDetail(slug);
    if (res.data.success) {
      return {
        status: true,
        data: res.data.data,
      };
    } else {
      console.log(res);
      return {
        status: false,
      };
    }
  } catch (err) {
    console.log(err);
    return {
      status: false,
    };
  }
};

export const getAllPages = async () => {
  try {
    let res = await footerServices.getAllPages();
    if (res.data.success) {
      return {
        status: true,
        data: res.data.data,
      };
    } else {
      console.log(res);
      return {
        status: false,
      };
    }
  } catch (err) {
    console.log(err);
    return {
      status: false,
    };
  }
};
