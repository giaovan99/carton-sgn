import Head from "next/head";
import React from "react";

export default function HomeLayout({ title, children }) {
  return (
    <>
      <Head>
        {title && (
          <>
            <title>{title}</title>
            <meta property="og:title" content={title} />
            <meta name="twitter:title" content={title} />
          </>
        )}
      </Head>
      <div>{children}</div>
    </>
  );
}
