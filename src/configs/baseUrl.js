import axios from "axios";

export const BASE_URL = "https://crm.thungcartonsgn.vn/api";
// export const BASE_URL = "http://crm.carton.hahoba.com/api/";
export const https = axios.create({
  baseURL: BASE_URL,
  timeout: 30000,
});
