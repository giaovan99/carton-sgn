"use client";
import { configureStore } from "@reduxjs/toolkit";
import productSlice from "./slice/productSlice";
import cartSlice from "./slice/cartSlice";
import settingSlice from "./slice/settingSlice";
import settingSpinnerAction from "./slice/spinnerSlice";

export const store = configureStore({
  reducer: { productSlice, cartSlice, settingSlice, settingSpinnerAction },
});
