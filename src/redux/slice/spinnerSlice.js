import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  actionSpinner: true,
};
export const settingSpinnerAction = createSlice({
  name: "settingSpinnerAction",
  initialState,
  reducers: {
    setActionSpinner: (state, action) => {
      state.actionSpinner = action.payload;
    },
  },
});

export const { setActionSpinner } = settingSpinnerAction.actions;

export default settingSpinnerAction.reducer;
