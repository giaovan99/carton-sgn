import { getLocalStorage, setLocalStorage } from "@/utils/localStorage";
import { createSlice } from "@reduxjs/toolkit";

// check localStorage & gán giá trị
let cloneCart = [];
if (typeof window !== "undefined") {
  if (localStorage.getItem("cart")) {
    cloneCart = getLocalStorage("cart");
  }
}

const initialState = {
  cart: cloneCart,
  totalQuantity: cloneCart.length,
};

export const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    setCart: (state, action) => {
      state.cart = action.payload;
      state.totalQuantity = state.cart.length;
      setLocalStorage("cart", JSON.stringify(state.cart));
    },
    setEmptyCart: (state, action) => {
      state.cart = [];
      state.totalQuantity = 0;
    },
  },
});

// Action creators are generated for each case reducer function
export const { setCart, setEmptyCart } = cartSlice.actions;

export default cartSlice.reducer;
