import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  arrProductList: [],
  arrProductType: [],
  arrNewProduct: [],
  arrBestSellerProduct: [],
  definePage: {
    title: "",
    id: -1,
  },
  productDetail: "",
};

export const productSlice = createSlice({
  name: "product",
  initialState,
  reducers: {
    setDefinePageTitle: (state, action) => {
      state.definePage = action.payload;
    },
    setProductDetail: (state, action) => {
      state.productDetail = action.payload;
    },
    setArrProductType: (state, action) => {
      state.arrProductType = action.payload;
    },
    setArrNewProduct: (state, action) => {
      state.arrNewProduct = action.payload;
    },
    setArrBestSellerProduct: (state, action) => {
      state.arrBestSellerProduct = action.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const {
  setDefinePageTitle,
  setProductDetail,
  setArrProductType,
  setArrBestSellerProduct,
  setArrNewProduct,
} = productSlice.actions;

export default productSlice.reducer;
