import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  settingObj: {},
};
export const settingSlice = createSlice({
  name: "setting",
  initialState,
  reducers: {
    setSettingInfo: (state, action) => {
      state.settingObj = action.payload;
    },
  },
});

export const { setSettingInfo } = settingSlice.actions;

export default settingSlice.reducer;
