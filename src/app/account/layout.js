export const metadata = {
  title: "CartonSGN - Thông tin của bạn",
  //   description: "",
};

export default function AccountLayout({
  children, // will be a page or nested layout
}) {
  return <section>{children}</section>;
}
