"use client";
import DetailAccountPage from "@/components/AccountPage/DetailAccountPage";
import DefinePage from "@/components/DefinePage/DefinePage";
import { setDefinePageTitle } from "@/redux/slice/productSlice";
import React from "react";
import { useEffect } from "react";
import { useDispatch } from "react-redux";

export default function PersonalInfo() {
  let dispatch = useDispatch();

  // set up define page
  let setDefinePage = () => {
    dispatch(setDefinePageTitle({ title: "Thông tin của tôi", id: -1 }));
  };

  useEffect(() => {
    setDefinePage();
  }, []);

  return (
    <section id="account">
      {/* Định nghĩa trang */}
      <DefinePage />
      {/* Thông tin và Hành động */}
      <DetailAccountPage />
    </section>
  );
}
