import { getAllPages } from "@/utils/footer";

const BASE_URL = "https://thungcartonsgn.vn";

export default async function sitemap() {
  let fixPage = [
    {
      url: BASE_URL + "/",
      lastModified: new Date(),
      changeFrequency: "weekly",
      priority: 1,
    },
    {
      url: BASE_URL + "/gioi-thieu",
      lastModified: new Date(),
      changeFrequency: "daily",
      priority: 0.7,
    },
    {
      url: BASE_URL + "/news-list",
      lastModified: new Date(),
      changeFrequency: "never",
      priority: 0.7,
    },
    {
      url: BASE_URL + "/contact",
      lastModified: new Date(),
      changeFrequency: "never",
      priority: 0.7,
    },
  ];
  //   let res = await productService.getSlugs();

  //   Lấy danh sách các trang thông tin
  let res = await getAllPages();
  console.log(res);
  let arr = [];
  if (res.status) {
    res.data.forEach((item) => {
      arr.push({
        url:
          BASE_URL +
          "/" +
          item.slug
            .replace(/&/g, "&amp;")
            .replace(/'/g, "&apos;")
            .replace(/"/g, "&quot;")
            .replace(/>/g, "&gt;")
            .replace(/</g, "&lt;"),

        lastModified: new Date(),
        changeFrequency: "never",
        priority: 0.8,
      });
    });
  }
  return [...fixPage, ...arr];
}
