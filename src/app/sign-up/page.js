import DefinePage from "@/components/DefinePage/DefinePage";
import FormSignupPage from "@/components/SignUpPage/FormSignupPage/FormSignupPage";
import React from "react";

export default function SignUpPage() {
  return (
    <section id="signUpPage">
      {/* Định nghĩa trang */}
      <DefinePage title={"Đăng ký tài khoản"} />
      {/* Đăng ký tài khoản */}
      <FormSignupPage />
    </section>
  );
}
