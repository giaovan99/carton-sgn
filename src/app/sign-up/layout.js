export const metadata = {
  title: "CartonSGN - Đăng ký tài khoản",
  //   description: "",
};

export default function SignUpLayout({
  children, // will be a page or nested layout
}) {
  return <section>{children}</section>;
}
