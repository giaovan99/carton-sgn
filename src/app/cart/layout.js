export const metadata = {
  title: "CartonSGN - Giỏ hàng",
  //   description: "",
};

export default function CartLayout({
  children, // will be a page or nested layout
}) {
  return <section>{children}</section>;
}
