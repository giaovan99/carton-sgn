"use client";
import DefinePage from "@/components/DefinePage/DefinePage";
import React, { useEffect, useState } from "react";
import RenderCartCartPage from "@/components/CartPage/RenderCartCartPage/RenderCartCartPage";
import RenderIntoMoneyCartPage from "@/components/CartPage/RenderIntoMoneyCartPage/RenderIntoMoneyCartPage";
import { useDispatch, useSelector } from "react-redux";
import { setDefinePageTitle } from "@/redux/slice/productSlice";

export default function CartPage() {
  const { cart } = useSelector((state) => state.cartSlice);
  let dispatch = useDispatch();
  const [totalCostOfGoods, setTotalCostOfGoods] = useState(0);
  const [shipmentFee, setShipmentFee] = useState(0);
  const [totalCost, setTotalCost] = useState(0);
  const [productArr, setProductArr] = useState([]);
  // check đăng nhập

  // api lấy phí vận chuyển
  let getShippingFee = async () => {
    // chưa đăng nhập
    let token = getLocalStorage("access_token");
    if (!token) {
      return setShipmentFee(0);
    } else {
    }
  };
  useEffect(() => {
    setProductArr(cart);
    let sumCOG = 0;
    let sumCost = 0;
    productArr.map((item) => {
      sumCOG += item.quantity * item.price;
    });
    sumCost = sumCOG + shipmentFee;
    setTotalCostOfGoods(sumCOG);
    setTotalCost(sumCost);
    dispatch(setDefinePageTitle({ title: "Giỏ hàng của tôi", id: "-1" }));
  }, [totalCostOfGoods, productArr, shipmentFee, totalCost, cart]);
  
  return (
    <section id="cartPage">
      {/* Định nghĩa trang */}
      <DefinePage />
      <div className="tb-padding-2">
        {/* Render giỏ hàng */}
        <RenderCartCartPage arr={productArr} setArr={setProductArr} />
        {/* Tinh tien */}
        <RenderIntoMoneyCartPage
          totalCostOfGoods={totalCostOfGoods}
          shipmentFee={shipmentFee}
          totalCost={totalCost}
          productArr={productArr}
        />
      </div>
    </section>
  );
}
