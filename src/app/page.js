"use client";
import BannerFooterHomePage from "@/components/HomePage/BannerFooterHomePage/BannerFooterHomePage";
import BannerHomePage from "@/components/HomePage/BannerHomePage/BannerHomePage";
import NewsHomePage from "@/components/HomePage/NewsHomePage/NewsHomePage";
import RenderProductList from "@/components/RenderProductList/RenderProductList";
import { productService } from "@/services/productService";
import {
  setArrBestSellerProduct,
  setArrNewProduct,
} from "@/redux/slice/productSlice";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { newsService } from "@/services/news";

export default function Home() {
  const [newsArr, setNewsArr] = useState();
  let [products, setProducts] = useState([]);

  const { arrProductType } = useSelector((state) => state.productSlice);

  const { arrBestSellerProduct, arrNewProduct } = useSelector(
    (state) => state.productSlice
  );
  const dispatch = useDispatch();
  useEffect(() => {
    // Lấy danh sách sản phẩm mới
    getArrNewProduct();
    // Lấy danh sách sản phẩm bán chạy
    getArrBestSellerProduct();
    // Lấy danh sách tin tức
    getNewsArr();
  }, []);

  // lấy danh sách sản phẩm mới
  let getArrNewProduct = async () => {
    const res = await productService.top("new");
    if (res.data.code == 200) {
      dispatch(setArrNewProduct(res.data.data));
    }
  };

  // lấy danh sách sản phẩm bán chạy
  let getArrBestSellerProduct = async () => {
    const res = await productService.top("sale");
    if (res.data.code == 200) {
      dispatch(setArrBestSellerProduct(res.data.data));
    }
  };

  // lấy danh sách tin tức
  let getNewsArr = async () => {
    let res = await newsService.list({
      limit: 3,
      page: 1,
      keywords: "",
      product_category_id: "",
    });
    if (res.data.success == true) {
      setNewsArr(res.data.data);
    }
  };

  let renderListProduct = async () => {
    let res1 = await Promise.all(
      arrProductType.map(async (cataloge, index) => {
        // console.log(index);
        let res = await productService.list({
          limit: 12,
          page: 1,
          product_category_id: cataloge.id,
        });
        // console.log(res);
        if (res.data.success) {
          return (
            <RenderProductList
              title={cataloge.name}
              arr={res.data.data}
              key={index}
            />
          );
        }
      })
    );
    // console.log(res1);
    setProducts(res1);
  };

  // console.log(products);

  useEffect(() => {
    if (arrProductType.length > 0) {
      renderListProduct();
    }
  }, [arrProductType.length]);

  // console.log(arrProductType);
  return (
    <section id="homePage">
      {/* banner */}
      <BannerHomePage />
      {/* sản phẩm mới */}
      <RenderProductList title={"Sản phẩm mới"} arr={arrNewProduct} />
      {/* sản phẩm bán chạy */}
      <div className="sanPhamBanChay">
        <RenderProductList
          title={"Sản phẩm bán chạy"}
          arr={arrBestSellerProduct}
        />
      </div>
      {/* Danh sách sản phẩm theo catology */}
      {arrProductType.length > 0 && (
        <div className="danhSachSanPham">{products}</div>
      )}

      {/* tin tức */}
      {newsArr && <NewsHomePage newsArr={newsArr} />}
      {/* footer-banner */}
      <BannerFooterHomePage />
    </section>
  );
}
