export const metadata = {
  title: "CartonSGN - Tin tức",
};

export default function NewsLayout({
  children, // will be a page or nested layout
}) {
  return <section>{children}</section>;
}
