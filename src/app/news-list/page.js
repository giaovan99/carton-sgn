"use client";
import DefinePage from "@/components/DefinePage/DefinePage";
import ListNews from "@/components/NewsListPage/ListNews";
import { setDefinePageTitle } from "@/redux/slice/productSlice";
import React, { useEffect } from "react";
import { useDispatch } from "react-redux";

export default function Page() {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(setDefinePageTitle({ title: "DANH SÁCH TIN TỨC", id: -1 }));
  }, []);

  return (
    <section id="newsListPage">
      {/* Định nghĩa trang */}
      <DefinePage />
      {/* Danh sách tin tức */}
      <ListNews />
    </section>
  );
}
