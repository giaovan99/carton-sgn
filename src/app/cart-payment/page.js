"use client";
import FormCartPayment from "@/components/CartPaymentPage/FormCartPayment/FormCartPayment";
import DefinePage from "@/components/DefinePage/DefinePage";
import { setDefinePageTitle } from "@/redux/slice/productSlice";
import { message } from "antd";
import { useRouter } from "next/navigation";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

export default function CartPayment() {
  let dispatch = useDispatch();
  let router = useRouter();

  // set up define page
  let setDefinePage = () => {
    dispatch(setDefinePageTitle({ title: "Thanh toán", id: -1 }));
  };

  // check giỏ hàng
  const { cart } = useSelector((state) => state.cartSlice);
  useEffect(() => {
    if (cart.length == 0) {
      message.error("Giỏ hàng rỗng, vui lòng chọn sản phẩm để đặt hàng");
      router.push("/");
    }
  }, []);

  useEffect(() => {
    setDefinePage();
  }, []);
  return (
    <section id="cart-payment">
      {/* Định nghĩa trang */}
      <DefinePage />
      {/* Thông tin trang thanh toán */}
      <FormCartPayment />
    </section>
  );
}
