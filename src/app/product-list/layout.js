import { Suspense } from "react";

export const metadata = {
  title: "CartonSGN - Danh sách sản phẩm",
  //   description: "",
};

export default function ProductListLayout({
  children, // will be a page or nested layout
}) {
  return (
    <section>
      <Suspense fallback={<p>Loading...</p>}>{children}</Suspense>
    </section>
  );
}
