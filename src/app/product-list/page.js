"use client";
import DefinePage from "@/components/DefinePage/DefinePage";
import RenderProductListPage from "@/components/ProductListPage/RenderProductListPage/RenderProductListPage";
import { setDefinePageTitle } from "@/redux/slice/productSlice";
import { productService } from "@/services/productService";
import { useRouter, useSearchParams } from "next/navigation";
import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

// export const dynamic = "force-dynamic";
// export const revalidate = "force-cache";

export default function ProductListPage() {
  const searchParams = useSearchParams();
  let categoryId = searchParams.get("categoryId");
  let keywords = searchParams.get("keywords");

  let dispatch = useDispatch();
  const [arrType, setArrType] = useState([]);
  const [page, setPage] = useState(1);
  const [sort_order, setSort_order] = useState("");
  const [total, setTotal] = useState();
  let [selected, setSelected] = useState("Mặc định");

  useEffect(() => {
    getDefinePage();
    getProductArr(page);
  }, [categoryId, page, keywords, sort_order]);

  useEffect(() => {
    if (selected === "Mặc định") {
      setSort_order("");
    } else if (selected === "Giá tăng dần") {
      setSort_order("asc");
    } else if (selected === "Giá giảm dần") {
      setSort_order("desc");
    }
  }, [selected]);

  let getProductArr = async (page) => {
    if (!keywords) {
      keywords = "";
    }
    let res = await productService.list({
      limit: 12,
      page: page,
      keywords: keywords,
      product_category_id: categoryId,
      sort_field: sort_order ? "price" : "",
      sort_order: sort_order,
    });

    if (res.data.code == 200) {
      setArrType(res.data.data);
      setTotal(res.data.total);
    }
  };

  let getDefinePage = async () => {
    if (categoryId > 0) {
      let res = await productService.productCategory();
      if (res.data.code == 200) {
        let index = res.data.data.findIndex(
          (product) => product.id == categoryId
        );
        dispatch(
          setDefinePageTitle({
            title: res.data.data[index].name,
            id: res.data.data[index].name,
          })
        );
      }
    } else {
      dispatch(
        setDefinePageTitle({
          title: "Tất cả sản phẩm",
          id: -1,
        })
      );
    }
  };

  return (
    <section id="productListPage">
      {/* Định nghĩa trang */}
      <DefinePage />
      {/* RenderProducts */}
      <RenderProductListPage
        arr={arrType}
        page={page}
        setPage={setPage}
        total={total}
        selected={selected}
        setSelected={setSelected}
      />
    </section>
  );
}

export const dynamic = "force-static";
