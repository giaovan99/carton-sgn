export const metadata = {
  title: "CartonSGN - Liên hệ",
  description: "",
};

export default function LoginLayout({
  children, // will be a page or nested layout
}) {
  return <section>{children}</section>;
}
