"use client";
import ContactDetail from "@/components/ContactPage/ContactDetail/ContactDetail";
import DefinePage from "@/components/DefinePage/DefinePage";
import { setDefinePageTitle } from "@/redux/slice/productSlice";
import React, { useEffect } from "react";
import { useDispatch } from "react-redux";

export default function Page() {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(setDefinePageTitle({ title: "Liên hệ", id: -1 }));
  }, []);

  return (
    <section id="introPage">
      {/* Định nghĩa trang */}
      <DefinePage />
      {/* */}
      <ContactDetail />
    </section>
  );
}
