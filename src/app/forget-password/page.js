import DefinePage from "@/components/DefinePage/DefinePage";
import FormForgetPasswordPage from "@/components/ForgetPasswordPage/FormForgetPasswordPage/FormForgetPasswordPage";
import React from "react";

export default function ForgetPassword() {
  return (
    <section id="forgetPasswordPage">
      {/* Định nghĩa trang */}
      <DefinePage title={"Khôi phục mật khẩu"} />
      {/* Đăng ký tài khoản */}
      <FormForgetPasswordPage />
    </section>
  );
}
