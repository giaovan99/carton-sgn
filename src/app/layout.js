import Header from "@/components/Header/Header";
import "../styles/globals.css";
import Footer from "@/components/Footer/Footer";
import Menu from "@/components/Menu/Menu";
import { Providers } from "@/redux/provider";
import { store } from "@/redux/store";
import { getSettingInfo } from "@/utils/setting";
import Script from "next/script";
import CallNow from "@/components/fixedItems/CallNow";
import ChatZalo from "@/components/fixedItems/ChatZalo";

let favicon;

export const metadata = async () => {
  let res = await getSettingInfo();
  if (res.status) {
    favicon = res.data.favicon;
    return {
      title: res.data.seo_title,
      description: res.data.seo_description,
      image: res.data.logo,
    };
  } else {
    return {
      title: "CartonSGN",
      description: "",
    };
  }
};

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <head>
        {/* <!-- Google Analytics --> */}
        {/* <Script
          dangerouslySetInnerHTML={{
            __html: `
           (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-N7KGVF45');
              `,
          }}
        ></Script> */}
        {/* Google Tag Manager */}
        <Script id="gtm-script" strategy="afterInteractive">
          {`
            (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-N7KGVF45');
          `}
        </Script>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link
          rel="preconnect"
          href="https://fonts.gstatic.com"
          crossOrigin="true"
        />
        <link
          rel="preload"
          href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap"
          as="style"
        />
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap"
        />
        <link rel="icon" href={favicon} sizes="any" />
      </head>
      <body suppressHydrationWarning={true}>
        <Providers store={store}>
          {/* Call Now */}
          <CallNow />
          <ChatZalo />
          <Header />
          <Menu />
          {children}
          <Footer />
        </Providers>
        {/* <!-- Google Tag Manager (noscript) --> */}
        <noscript>
          <iframe
            src="https://www.googletagmanager.com/ns.html?id=GTM-N7KGVF45"
            height="0"
            width="0"
            style={{ display: "none", visibility: "hidden" }}
          ></iframe>
        </noscript>
        {/* <!-- End Google Tag Manager (noscript) --> */}
      </body>
    </html>
  );
}
