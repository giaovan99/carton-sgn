export const metadata = {
  title: "CartonSGN - Đăng nhập",
  //   description: "",
};

export default function LoginLayout({
  children, // will be a page or nested layout
}) {
  return <section>{children}</section>;
}
