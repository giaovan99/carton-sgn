import DefinePage from "@/components/DefinePage/DefinePage";
import LoginCheck from "@/components/LoginPage/LoginCheck";
import React from "react";
export const metadata = {
  title: "Carton SGN - Đăng nhập",
  description: "...",
};
export default function LoginPage() {
  return (
    <section id="loginPage">
      {/* Định nghĩa trang */}
      <DefinePage />
      {/* Login Page */}
      <LoginCheck />
    </section>
  );
}
