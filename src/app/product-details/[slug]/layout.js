import { productService } from "@/services/productService";

export const generateMetadata = async ({ params }) => {
  let res = await productService.productDetail(params.slug);
  return {
    title: res.data.data?.name,
    description: res.data.data?.description,
  };
};

export default function ChiTietLayout({
  children, // will be a page or nested layout
}) {
  return <section>{children}</section>;
}
