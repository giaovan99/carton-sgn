"use client";
import DefinePage from "@/components/DefinePage/DefinePage";
import DiscriptionProductDetailPage from "@/components/ProductDetailsPage/DiscriptionProductDetailPage/DiscriptionProductDetailPage";
import InfoProductDetailPage from "@/components/ProductDetailsPage/InfoProductDetailPage/InfoProductDetailPage";
import RenderProductList from "@/components/RenderProductList/RenderProductList";
import { setDefinePageTitle } from "@/redux/slice/productSlice";
import { productService } from "@/services/productService";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import ImageGalleryProductDetailPage from "@/components/ProductDetailsPage/ImageGalleryProductDetailPage/ImageGalleryProductDetailPage";

export default function ProductDetailPage({ params }) {
  let dispatch = useDispatch();

  const { slug } = params;
  const { definePage } = useSelector((state) => state.productSlice);
  const [item, setItem] = useState({});
  const [arr, setArr] = useState([]);

  // lấy danh sách sản phẩm liên quan
  let getRelateProduct = async () => {
    let res = await productService.list({
      limit: 8,
      page: 1,
      keywords: "",
      product_category_id: item.product_category_id,
    });
    setArr(res.data.data);
  };

  // lấy thông tin sản phẩm
  let getProductObject = async () => {
    let res = await productService.productDetail(slug);
    // console.log(res);
    if (res.data.success == true) {
      setItem(res.data.data);
      dispatch(
        setDefinePageTitle({ title: res.data.data.name, id: res.data.data.id })
      );
    }
  };

  // lấy api sản phẩm & setItem & định nghĩa trang
  useEffect(() => {
    getProductObject();
  }, [slug]);

  // lấy danh sách sản phẩm liên quan
  useEffect(() => {
    getRelateProduct();
  }, [item]);

  return (
    <section id="productDetailPage">
      {/* Định nghĩa page */}
      <DefinePage />

      {/* Gallery và Thông tin */}
      <div className="gallery-info grid grid-cols-10 py-8 lg:py-14 space-y-4 lg:space-y-0 container-1 lg:space-x-[20%]">
        {/* Gallery */}
        <div className="d-flex justify-center gallery col-span-10 lg:col-span-4">
          {item && (
            <ImageGalleryProductDetailPage
              img={item?.image}
              gallery={item?.gallery}
            />
          )}
        </div>

        {/* Info */}
        {item && (
          <div className="info col-span-10 lg:col-span-6">
            <InfoProductDetailPage item={item} />
          </div>
        )}
      </div>

      {/* Mô tả */}
      <DiscriptionProductDetailPage script={item?.long_description} />

      {/* Sản phẩm liên quan */}
      <div className="bg-main-gray">
        <RenderProductList title={"Sản phẩm liên quan"} arr={arr} />
      </div>
    </section>
  );
}
