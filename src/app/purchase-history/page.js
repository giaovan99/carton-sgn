"use client";
import DefinePage from "@/components/DefinePage/DefinePage";
import RenderListOrder from "@/components/PurchaseHistoryPage/RenderListOrder";
import { setDefinePageTitle } from "@/redux/slice/productSlice";
import { useRouter } from "next/navigation";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

export default function PurchaseHistory() {
  let dispatch = useDispatch();
  let router = useRouter();

  // set up define page
  let setDefinePage = () => {
    dispatch(setDefinePageTitle({ title: "Lịch sử mua hàng", id: -1 }));
  };

  useEffect(() => {
    setDefinePage();
  }, []);
  return (
    <section id="cart-payment">
      {/* Định nghĩa trang */}
      <DefinePage />
      {/* Danh sách đơn hàng */}
      <RenderListOrder />
    </section>
  );
}
