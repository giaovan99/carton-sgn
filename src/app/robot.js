export default function robots() {
  return {
    rules: {
      userAgent: "*",
      allow: "/",
      disallow: "/private/",
    },
    sitemap: "https://thungcartonsgn.vn/sitemap.xml",
  };
}
