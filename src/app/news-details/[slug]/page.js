"use client";
import DefinePage from "@/components/DefinePage/DefinePage";
import ContactNewsDetails from "@/components/NewsDetailsPage/ContactNewsDetails";
import NewsDetailsItem from "@/components/NewsDetailsPage/NewsDetailsPage";
import { setDefinePageTitle } from "@/redux/slice/productSlice";
import { newsService } from "@/services/news";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";

export default function Page({ params }) {
  const { slug } = params;
  const dispatch = useDispatch();
  const [item, setItem] = useState();
  //   setTitlePage
  useEffect(() => {
    dispatch(setDefinePageTitle({ title: "TIN TỨC", id: -1 }));
  }, []);

  //   API lấy thông tin sản phẩm
  let getNewsDetails = async () => {
    let res = await newsService.detail(slug);
    if (res.data.success == true) {
      setItem(res.data.data);
    }
  };

  //  lần đầu load trang
  useEffect(() => {
    getNewsDetails();
  }, []);

  return (
    <section id="newsDetails">
      {/* Định nghĩa trang */}
      <DefinePage />

      {/* Chi tiết sản phẩm */}
      {item && <NewsDetailsItem item={item} />}

      {/* contact */}
      {/* <ContactNewsDetails /> */}
    </section>
  );
}
