import { newsService } from "@/services/news";

export const generateMetadata = async ({ params }) => {
  let res = await newsService.detail(params.slug);
  return {
    title: res.data.data?.name,
    description: res.data.data?.description,
  };
};

export default function ChiTietLayout({
  children, // will be a page or nested layout
}) {
  return <section>{children}</section>;
}
