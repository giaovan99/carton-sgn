import { getDetail } from "@/utils/footer";

export const generateMetadata = async ({ params }) => {
  let res = await getDetail(params.slug);
  if (res.status) {
    return {
      title: res.data?.name,
      description: res.data?.description,
    };
  }
};

export default function ChiTietLayout({
  children, // will be a page or nested layout
}) {
  return <section>{children}</section>;
}
