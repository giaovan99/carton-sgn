"use client";
import DefinePage from "@/components/DefinePage/DefinePage";
import { setDefinePageTitle } from "@/redux/slice/productSlice";
import { getDetail } from "@/utils/footer";
import { notFound, useRouter } from "next/navigation";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";

export default function Page({ params }) {
  const { slug } = params;
  const dispatch = useDispatch();
  const [data, setData] = useState();

  //   API lấy thông tin sản phẩm
  let getDetails = async () => {
    let res = await getDetail(slug);
    if (res.status) {
      setData(res.data);
      dispatch(setDefinePageTitle({ title: res.data.name, id: -1 }));
    } else {
      notFound();
    }
  };

  //  lần đầu load trang
  useEffect(() => {
    if (slug) {
      getDetails();
    }
  }, [slug]);

  return (
    <section id="newsDetails">
      {/* Định nghĩa trang */}
      <DefinePage />
      <div className="container-1 py-4">
        <div dangerouslySetInnerHTML={{ __html: data?.content }} />
      </div>
    </section>
  );
}
