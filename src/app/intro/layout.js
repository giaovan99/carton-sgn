import { Suspense } from "react";

export const metadata = {
  title: "CartonSGN - Giới thiệu",
  description: "",
};

export default function IntroLayout({
  children, // will be a page or nested layout
}) {
  return (
    <section>
      <Suspense fallback={<p>Loading...</p>}>{children}</Suspense>
    </section>
  );
}
