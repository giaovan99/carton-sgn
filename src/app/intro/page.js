"use client";
import DefinePage from "@/components/DefinePage/DefinePage";
import IntroDetail from "@/components/IntroPage/IntroDetail/IntroDetail";
import { setDefinePageTitle } from "@/redux/slice/productSlice";
import React, { useEffect } from "react";
import { useDispatch } from "react-redux";

export default function Page() {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(setDefinePageTitle({ title: "Giới thiệu", id: -1 }));
  }, []);

  return (
    <section id="introPage">
      {/* Định nghĩa trang */}
      <DefinePage title={"Giới thiệu"} />
      {/* Giới thiệu thông tin công ty */}
      <IntroDetail />
    </section>
  );
}
